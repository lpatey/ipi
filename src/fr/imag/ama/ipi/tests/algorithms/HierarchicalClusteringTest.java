package fr.imag.ama.ipi.tests.algorithms;

import junit.framework.TestCase;

import org.junit.Test;

import fr.imag.ama.ipi.algorithms.HierarchicalClustering;
import fr.imag.ama.ipi.tools.ClusteringResult;

public class HierarchicalClusteringTest extends TestCase {

	@Test
	public final void testCompute() {

		float[][] sim = {
				{0.6f},
				{0.3f, 0.4f},
				{0.8f, 0.7f, 0.2f},
				{ 0.1f, 0.3f, 0.3f, 0.3f}
		};

		ClusteringResult r = HierarchicalClustering.compute( sim );
		
		System.out.print( "Order : ");
		for( int order : r.getOrder() ) {
			System.out.print( order + " " );
		}
		System.out.println();
		
		System.out.println( "Merge :");
		for( int[] row : r.getMerge() ) {
			System.out.println( row[0] + " " + row[1] + " " + row[2] );
		}

	
	}

}
