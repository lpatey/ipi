package fr.imag.ama.ipi.tests.algorithms;

import junit.framework.TestCase;

import static org.junit.Assert.*;
import org.junit.Test;

import fr.imag.ama.ipi.algorithms.HungarianAlgorithm;
import fr.imag.ama.ipi.algorithms.families.MaximalCostAssignmentAlgorithm;

public class HungarianAlgorithmTest extends TestCase {

	@Test
	public final void testCompute() {
		
		MaximalCostAssignmentAlgorithm hungarianAlgorithm = new HungarianAlgorithm();
		
		double[][] matrix = {
				{ 5, 3, 3 },
				{ 4, 3, 3 },
				{ 3, 3, 4 }
		};
		
		int[][] result = { 
				{ 0, 0 },
				{ 1, 1 },
				{ 2, 2 } 
		};
			
		assertArrayEquals( result, hungarianAlgorithm.compute(matrix) );

	}

}
