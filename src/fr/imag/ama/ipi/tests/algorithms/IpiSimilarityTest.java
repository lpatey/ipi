package fr.imag.ama.ipi.tests.algorithms;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import junit.framework.TestCase;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import fr.imag.ama.ipi.algorithms.AssignmentAdapter;
import fr.imag.ama.ipi.algorithms.GreedyAlgorithm;
import fr.imag.ama.ipi.algorithms.HungarianAlgorithm;
import fr.imag.ama.ipi.algorithms.IpiSimilarity;
import fr.imag.ama.ipi.algorithms.families.GraphMatchingAlgorithm;
import fr.imag.ama.ipi.algorithms.families.MaximalCostAlgorithm;
import fr.imag.ama.ipi.algorithms.families.SimilarityAlgorithm;
import fr.imag.ama.ipi.beans.Environment;
import fr.imag.ama.ipi.beans.Graph;
import fr.imag.ama.ipi.tools.SimilarityComparator;
import fr.imag.ama.ipi.tools.Verbose;
import fr.imag.ama.ipi.tools.io.Reader;
import fr.imag.ama.ipi.tools.io.chemistry.SDFReader;

public class IpiSimilarityTest extends TestCase {
	
	private static List<Graph> list;
	public static long N_ITERATIONS = 0;
	public static long N_ITEMS = 0;
	public static long N_SIMILARITY = 0;
	public static long N_GRAPHS = 0;
	public static long N_VERTICES = 0;
	public static long N_EDGES = 0;
	public static long N_SIMILARITY_EDGE = 0;
	
	private int[][] orderRelationExample = {
			{1,10},{1,24},{1,3},{1,14},{1,4},{1,6},{1,11},{1,2},{5,20},{5,23},{0,1},{1,22},
			{5,7},{1,20},{1,23},{12,20},{12,23},{15,16},{15,17},{19,23},{1,15},{1,18},{12,15},
			{5,24},{5,16},{5,17},{19,24},{5,9},{1,7},{16,19},{17,19},{9,19},{9,15},{4,16},
			{4,17},{4,12},{12,24},{6,19},{11,19},{21,24},{1,5},{4,5},{9,12},{1,19},{20,24},
			{6,16},{6,17},{11,16},{11,17},{13,23},{15,24},{12,18},{2,5},{5,10},{13,24},
			{12,16},{12,17},{4,23},{16,21},{17,21},{15,23},{6,9},{6,20},{6,23},{9,11},{11,20},
			{11,23},{0,5},{1,12},{22,23},{6,18},{11,15},{11,18},{19,22},{9,13},{13,16},
			{13,17},{21,23},{4,24},{2,12},{10,12},{6,7},{11,13},{5,18},{16,22},{17,22},{4,9},
			{1,21},{2,19},{10,19},{15,19},{18,19},{0,12},{7,16},{7,17},{4,6},{4,11},{6,24},
			{11,24},{2,15},{19,20},{18,24},{2,21},{1,13},{9,21},{5,8},{9,22},{20,22},{0,19},
			{15,20},{3,12},{18,22},{0,21},{16,24},{17,24},{7,9},{7,19},{12,19},{0,13},{7,22},
			{10,20},{14,16},{14,17},{2,16},{2,17},{1,9},{12,14},{2,7},{2,13},{7,10},{10,13},
			{4,22},{22,24},{0,20},{3,16},{3,17},{9,24},{23,24},{0,16},{0,17},{8,23},{1,16},
			{1,17},{8,16},{8,17},{2,6},{2,11},{6,10},{10,11},{8,18},{10,15},{0,7},{3,24},
			{0,6},{0,11},{6,21},{11,21},{16,20},{16,23},{17,20},{17,23},{8,9},{9,14},{14,23},
			{10,21},{0,15},{14,18},{16,18},{17,18},{8,24},{14,24},{7,18},{13,18},{2,24},{2,22},
			{10,22},{18,21},{7,24},{12,21},{2,4},{4,10},{4,19},{9,20},{7,12},{6,8},{0,24},
			{0,22},{9,18},{20,21},{13,19},{0,4},{1,8},{7,23},{13,20},{3,18},{5,11},{2,8},
			{8,10},{8,19},{7,15},{7,21},{7,20},{9,23},{0,8},{8,22},{13,22},{4,20},{10,24},
			{18,20},{18,23},{4,21},{6,12},{8,21},{3,19},{2,14},{10,14},{2,20},{2,23},{12,22},
			{5,6},{4,7},{3,11},{2,18},{10,18},{8,20},{5,15},{0,14},{21,22},{5,22},{0,9},{0,23},
			{14,19},{6,15},{10,16},{10,17},{7,13},{11,14},{3,9},{0,18},{15,22},{6,13},{7,11},
			{3,5},{11,12},{4,8},{3,23},{8,12},{2,9},{12,13},{0,3},{4,18},{20,23},{5,14},{8,15},
			{9,16},{9,17},{8,13},{11,22},{3,22},{9,10},{15,21},{2,3},{14,22},{0,10},{14,20},
			{8,11},{3,10},{7,8},{6,22},{13,15},{4,13},{10,23},{5,12},{3,21},{14,21},{3,15},
			{2,10},{19,21},{4,14},{3,6},{14,15},{6,14},{5,21},{3,4},{7,14},{3,20},{5,19},
			{13,21},{3,8},{4,15},{5,13},{3,7},{3,13},{8,14},{13,14},{15,18},{16,17},{3,14},
			{0,2},{6,11}
			};
	
	@Before
	public void setUp() throws Exception {

		/*File source = new File( "src/fr/imag/ama/similarity/tests/data/dhfr_oak.sdf" );
		Reader reader = new SDFReader();

		N_GRAPHS = 0;
		N_VERTICES = 0;
		N_EDGES = 0;
		Environment env = reader.read( source );
		list = env.getGraphs();
		System.out.println( "N graphs : " + N_GRAPHS );
		System.out.println( "N vertices : " + N_VERTICES );
		System.out.println( "N edges : " + N_EDGES );*/
	
	}
	
	@AfterClass
	public static void tearDownAfterClass() {
		list = null;
	}

	private double[][] getSimilarityMatrix( float threshold, boolean verbose ) {
		
		MaximalCostAlgorithm adapter = new AssignmentAdapter( new HungarianAlgorithm() );
		GraphMatchingAlgorithm greedy = new GreedyAlgorithm();
		SimilarityAlgorithm ipi = new IpiSimilarity( adapter, greedy, threshold );
		

		int length = list.size();
		double[][] result = new double[ length ][ length ];
		
		double time = System.currentTimeMillis();
		for( int i=0; i<length; i++ ) {
			result[i][i] = 1;
			for( int j=i+1; j<length; j++ ) {               
				
				Graph a = list.get(i);
				Graph b = list.get(j);
				double degree = ipi.getSimilarityDegree( a, b );
				double sim = Math.round( 1000000*degree ) /1000000.0;
				result[i][j] = result[j][i] = sim;
			
			}		
		}
		
		time = System.currentTimeMillis() - time;
		
		if( verbose ) {
			System.out.println( "Time : " + time + " ms ( " + time/(length*(length-1)) + "/ inclusion ) " );
		}
		
		
		return result;
	}
	
	private int[][] getOrderedResult( double[][] matrix ) {
		SimilarityComparator comparator = new SimilarityComparator( matrix );
		ArrayList<int[]> orderedSet = new ArrayList<int[]>();
		
		for( int i=0; i<list.size(); i++ ) {
			for( int j=i+1; j<list.size(); j++ ) {
				orderedSet.add( new int[]{ i, j } );
			}
		}
	
		Collections.sort( orderedSet, comparator );

		return orderedSet.toArray(new int[orderedSet.size()][] );
		
	}
	
	
	@Test
	public void testOrderRelation() {
		
		int[][] resultArray = getOrderedResult( getSimilarityMatrix( 0.001f, false ) );
		
		int[] distance = getDistance( orderRelationExample, resultArray );
		System.out.println( "Distance : " + distance[0] + " N elements : " + distance[1]  );
		assertEquals( 0, distance[0] );
		assertEquals( 0, distance[1] );
	}
	
	@Test
	public void testThreshold() {
		
		float threshold = 1;
		for( int i=0; i<10; i++ ) {
			
			N_ITERATIONS = 0;
			N_ITEMS = 0;
			N_SIMILARITY = 0;
			N_SIMILARITY_EDGE = 0;
			
			int[][] resultArray = getOrderedResult( getSimilarityMatrix( threshold, true ) );
			
			System.out.println( "N iterations : " + N_ITERATIONS );
			System.out.println( "N items : " + N_ITEMS );
			System.out.println( "Avg : " + (N_ITERATIONS*1.0/N_ITEMS) );
			System.out.println( "N sim : " + (N_SIMILARITY) );
			System.out.println( "N sim edge : " + (N_SIMILARITY_EDGE) );
			int[] distance1 = getDistance( orderRelationExample, resultArray );
			int[] distance2 = getDistance( resultArray, orderRelationExample );
			
			System.out.println( "Distance : " + distance1[0] + " N elements : " + distance1[1]  );
			System.out.println( "Distance : " + distance2[0] + " N elements : " + distance2[1]  );
			
			System.out.println();
			
			threshold /= 10;
			
		}
	}
	
	private int[] getDistance( int[][] a, int[][] b ) {
		
		int[][] a2 = new int[a.length][];
		for( int i=0; i<a.length; i++) {
			a2[i] = a[i];
		}
		
		int[][] b2 = new int[b.length][];
		for( int i=0; i<b.length; i++) {
			b2[i] = b[i];
		}
		
		int distance = 0;
		int nDifferences = 0;
		for( int i=0; i<a2.length; i++ ) {
			
			int j=0;
			while( a2[i][0] != b2[i+j][0] || a2[i][1] != b2[i+j][1] ) {
				j++;				
			}
			
			if( j!= 0 ) {
				nDifferences++;
				distance+= j;
				while( j>0 ) {
					b2[i+j] = b2[i+j-1];
					j--;
				}
				b2[i] = a2[i];
			}
		}
		return new int[]{ distance, nDifferences };
	}
	
	@Test
	public void testGlobalMatrix() {
		
		double[][] matrix = {
				{1.0, 0.283613, 0.915033, 0.575368, 0.475936, 0.356506, 0.435294, 0.431373, 0.501961, 0.535068 },
				{0.283613, 1.0, 0.27381, 0.232143, 0.243506, 0.330087, 0.257143, 0.309524, 0.478571, 0.407967 },
				{0.915033, 0.27381, 1.0, 0.618056, 0.459596, 0.343434, 0.422222, 0.416667, 0.488889, 0.564103 },
				{0.575368, 0.232143, 0.618056, 1.0, 0.701705, 0.556818, 0.675, 0.729167, 0.710417, 0.543269 },
				{0.475936, 0.243506, 0.459596, 0.701705, 1.0, 0.333333, 0.381818, 0.522727, 0.560606, 0.374126 },
				{0.356506, 0.330087, 0.343434, 0.556818, 0.333333, 1.0, 0.52197, 0.287879, 0.387879, 0.309441 },
				{0.435294, 0.257143, 0.422222, 0.675, 0.381818, 0.52197, 1.0, 0.366667, 0.466667, 0.353846 },
				{0.431373, 0.309524, 0.416667, 0.729167, 0.522727, 0.287879, 0.366667, 1.0, 0.65, 0.400641 },
				{0.501961, 0.478571, 0.488889, 0.710417, 0.560606, 0.387879, 0.466667, 0.65, 1.0, 0.439744 },
				{0.535068, 0.407967, 0.564103, 0.543269, 0.374126, 0.309441, 0.353846, 0.400641, 0.439744, 1.0 }
				};
			
		double[][] result = getSimilarityMatrix( 0.0f, true );
		int length = 10;
		for( int i=0; i<length; i++ ) {	
			for( int j=i+1; j<length; j++ ) { 
				assertEquals( matrix[i][j], result[i][j] );
			}
		}
	
	}
	
	@Test
	public void testSimilarityVariation() {
		
		File matrixFile1 = new File( "/home/ludovic/Bureau/IPIComputer/next/InFarmatik_standardized_full.matrix" );
		File matrixFile2 = new File( "/home/ludovic/Bureau/IPIComputer/next/InFarmatik_standardized_full2.matrix" );
		int size = 727;
		long nSimilarities = 0;
		double average = 0;
		double variation = 0;
		
		float[][] similarityMatrix1 = new float[size][];
		float[][] similarityMatrix2 = new float[size][];
		
		try {
			loadSimilarityMatrix( matrixFile1, similarityMatrix1 );
			loadSimilarityMatrix(matrixFile2, similarityMatrix2);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		for( int i=0; i<similarityMatrix1.length; i++ ) {
			float[] similarityVector1 = similarityMatrix1[i];
			float[] similarityVector2 = similarityMatrix2[i];
			for( int j=0; j<similarityVector1.length; j++ ) {
				float diff = Math.abs(similarityVector1[j] - similarityVector2[j]);
				average += diff;
				variation = Math.max( variation, diff );
				nSimilarities++;
			}
		}
		
		System.out.println( "Average : " + ( average/nSimilarities) );
		
		System.out.println( "Max variation : " + (variation) );
		
		System.exit(0);
		
	}
	
	private void loadSimilarityMatrix( File matrixFile, float[][] similarityMatrix ) throws FileNotFoundException {
		FileReader reader = new FileReader( matrixFile );
		LineNumberReader lnReader = new LineNumberReader( reader );
		String line;
		int i=0;
		
		try {
		
			while( (line = lnReader.readLine() ) != null ) {
				String[] texts = line.trim().split( " " );
				float[] numbers = new float[i+1];
				for( int j=0; j<texts.length; j++ ) {
					numbers[i] = Float.parseFloat( texts[j] );
				}
				similarityMatrix[i] = numbers;
				if( texts.length < i+1 ) {
					System.out.println( "Error : suppress last line from similarity matrix" );
					System.out.println( "Line : " + (i+1) );
					System.exit( 1 );
				}
				
				i++;
			}
			
			lnReader.close();
			reader.close();
		
		}
		catch( IOException e )  {
			e.printStackTrace();
		}
		
	}

}
