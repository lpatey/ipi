package fr.imag.ama.ipi.tests;

import fr.imag.ama.ipi.tests.algorithms.HierarchicalClusteringTest;
import fr.imag.ama.ipi.tests.algorithms.HungarianAlgorithmTest;
import fr.imag.ama.ipi.tests.algorithms.IpiSimilarityTest;
import fr.imag.ama.ipi.tests.tools.io.GenericReaderTest;
import fr.imag.ama.ipi.tests.tools.io.IRLReaderTest;
import fr.imag.ama.ipi.tests.tools.io.IRLWriterTest;
import fr.imag.ama.ipi.tests.tools.io.chemistry.SDFReaderTest;
import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {
	
	public static Test suite() {
		
		TestSuite suite = new TestSuite( "All tests" );
		
		//suite.addTestSuite( CompoundReaderTest.class );
		suite.addTestSuite( SDFReaderTest.class );
		suite.addTestSuite( IRLWriterTest.class );
		suite.addTestSuite( IRLReaderTest.class );
		suite.addTestSuite( GenericReaderTest.class );
		suite.addTestSuite( HungarianAlgorithmTest.class );
		suite.addTestSuite( IpiSimilarityTest.class );
		suite.addTestSuite( HierarchicalClusteringTest.class );

		return suite;
		
	}

}
