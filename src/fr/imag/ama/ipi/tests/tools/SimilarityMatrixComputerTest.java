package fr.imag.ama.ipi.tests.tools;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

import fr.imag.ama.ipi.beans.Environment;
import fr.imag.ama.ipi.tools.SimilarityMatrixComputer;
import fr.imag.ama.ipi.tools.io.Reader;
import fr.imag.ama.ipi.tools.io.chemistry.SDFReader;

public class SimilarityMatrixComputerTest extends TestCase implements ActionListener {
	
	private static Environment environment;
	private String source = "src/fr/imag/ama/ipi/tests/data/dhfr_oak.sdf";
	
	
	@Before
	public void setUp() throws Exception {

		Reader reader = new SDFReader();

		environment = reader.read( new File( source ) );
	
	}


	
	@Test
	public void testGetNextPoint() {
		SimilarityMatrixComputer computer = new SimilarityMatrixComputer();
		computer.setInput( source );
		int length = environment.getGraphs().size();

		for( int i=0; i<length; i++ ) {	
			for( int j=0; j<length; j++ ) {
				int[] point = computer.getNextPoint();
				System.out.println( "(" + point[0] + "," + point[1] + ") (" + i + " ," + j+ ")" );
				assertEquals( point[0], i );
				assertEquals( point[1], j );
			}
		}
	}
	
	@Test
	public void testCompute() {
		SimilarityMatrixComputer computer = new SimilarityMatrixComputer();
		computer.setInput( source );
		computer.addActionListener( this );
		computer.setIrln( true );
		computer.setClustering( true );
		
		System.out.println( "Start" );
		try {
			computer.compute();
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
	}

	public void actionPerformed(ActionEvent e) {
		System.out.println( "End" );
		
	}
}
