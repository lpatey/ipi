package fr.imag.ama.ipi.tests.tools;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.imag.ama.ipi.tools.ArrayParser;
import fr.imag.ama.ipi.tools.InvalidArrayException;

public class ArrayParserTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testParse() {
		
		String raw = "{{Hello,World },yes,[hi]}";
		try {
			String[] result = ArrayParser.parse( raw );
			assertEquals( "{Hello,World }", result[0] );
			assertEquals( "yes", result[1] );
			assertEquals( "[hi]", result[2] );
		}
		catch( InvalidArrayException e ) {
			e.printStackTrace();
			fail( e.getMessage() );
		}

	}

}
