package fr.imag.ama.ipi.tests.tools.io;

import java.io.File;
import java.util.List;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.imag.ama.ipi.beans.Edge;
import fr.imag.ama.ipi.beans.Environment;
import fr.imag.ama.ipi.beans.Graph;
import fr.imag.ama.ipi.beans.Vertex;
import fr.imag.ama.ipi.tools.io.IRLReader;
import fr.imag.ama.ipi.tools.io.IRLWriter;
import fr.imag.ama.ipi.types.Enum;
import fr.imag.ama.ipi.types.Integer;

public class IRLReaderTest extends TestCase {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testRead() {
		
		File file = new File( "src/fr/imag/ama/ipi/tests/data/er_oak.irl" );
		File dest = new File( "src/fr/imag/ama/ipi/tests/data/er_oak2.irl" );
		IRLReader reader = new IRLReader();
		IRLWriter defaultWriter = new IRLWriter();
		
		try {
			
			Environment env = reader.read( file );
			defaultWriter.write( dest, env );
			
			List<Graph> list = env.getGraphs();
			assertEquals( 10, list.size() );
			
			Graph molecule = list.get(0);
			
			assertEquals("lit-1", molecule.getName() );
			assertEquals( 21, molecule.getVertices().length );
			assertEquals( 24, molecule.getEdges().length );
			
			Vertex atom = molecule.getVertices()[0];
			assertEquals( "C", ((Enum) atom.getValues()[0]).getName() );
			
			Edge bond = (Edge) molecule.getEdges()[0];
			assertEquals( 4, ((Integer) bond.getValues()[0]).getValue().intValue() );
			assertEquals( atom, bond.getVertices()[0] );
			
		}
		catch( Exception e ) {
			e.printStackTrace();
			fail( e.getMessage() );
		}

	}

}
