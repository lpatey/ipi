package fr.imag.ama.ipi.tests.tools.io;

import java.io.File;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.imag.ama.ipi.tools.io.IRLWriter;
import fr.imag.ama.ipi.tools.io.Reader;
import fr.imag.ama.ipi.tools.io.Writer;
import fr.imag.ama.ipi.tools.io.chemistry.SDFReader;

public class IRLWriterTest extends TestCase {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testWrite() {
		File source = new File( "src/fr/imag/ama/ipi/tests/data/er_oak.sdf" );
		File dest = new File( "src/fr/imag/ama/ipi/tests/data/er_oak.irl" );
		Reader reader = new SDFReader();
		Writer writer = new IRLWriter();
		
		try {
			writer.write( dest, reader.read( source ) );
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
	}

}
