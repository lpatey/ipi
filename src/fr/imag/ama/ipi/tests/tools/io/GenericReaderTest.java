package fr.imag.ama.ipi.tests.tools.io;

import java.io.File;
import java.util.List;

import junit.framework.TestCase;

import org.junit.Test;

import fr.imag.ama.ipi.beans.Environment;
import fr.imag.ama.ipi.beans.Graph;
import fr.imag.ama.ipi.tools.io.GenericReader;

public class GenericReaderTest extends TestCase {

	@Test
	public void testIRLRead() {
		
		File file = new File( "src/fr/imag/ama/ipi/tests/data/er_oak.irl" );
		GenericReader reader = new GenericReader();
		
		try {
			
			Environment env = reader.read( file );
			
			List<Graph> list = env.getGraphs();
			assertEquals( 10, list.size() );
			
		}
		catch( Exception e ) {
			e.printStackTrace();
			fail( e.getMessage() );
		}
	}
	
	@Test
	public void testSDFRead() {
		
		File file = new File( "src/fr/imag/ama/ipi/tests/data/er_oak.sdf" );
		GenericReader reader = new GenericReader();
		
		try {
			
			Environment env = reader.read( file );
			
			List<Graph> list = env.getGraphs();
			assertEquals( 10, list.size() );
			
		}
		catch( Exception e ) {
			e.printStackTrace();
			fail( e.getMessage() );
		}
	}

}
