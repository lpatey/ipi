package fr.imag.ama.ipi.tests.tools.io.chemistry;


import java.io.File;
import java.util.List;

import junit.framework.TestCase;

import org.junit.Test;

import fr.imag.ama.ipi.beans.Edge;
import fr.imag.ama.ipi.beans.Environment;
import fr.imag.ama.ipi.beans.Graph;
import fr.imag.ama.ipi.beans.Vertex;
import fr.imag.ama.ipi.tools.io.Reader;
import fr.imag.ama.ipi.tools.io.chemistry.SDFReader;
import fr.imag.ama.ipi.types.Enum;
import fr.imag.ama.ipi.types.Integer;

public class SDFReaderTest extends TestCase {
	
	

	@Test
	public final void testRead() {
		
		File source = new File( "src/fr/imag/ama/ipi/tests/data/er_oak.sdf" );
		Reader reader = new SDFReader();
		
		try {
			
			Environment env = reader.read( source );
			List<Graph> list = env.getGraphs();
			assertEquals( 10, list.size() );
			
			Graph molecule = list.get(0);
			
			assertEquals("lit-1", molecule.getName() );
			assertEquals( 21, molecule.getVertices().length );
			assertEquals( 24, molecule.getEdges().length );
			
			Vertex atom = molecule.getVertices()[0];
			assertEquals( "C", ((Enum) atom.getValues()[0]).getName() );
			
			Edge bond = (Edge) molecule.getEdges()[0];
			assertEquals( 4, ((Integer) bond.getValues()[0]).getValue() );
			assertEquals( atom, bond.getVertices()[0] );

		
		}
		catch( Exception e ) {
			e.printStackTrace();
			fail( e.getMessage() );
		}
	}
	
	@Test
	public final void testRead2() {
		
		File source = new File( "src/fr/imag/ama/ipi/tests/data/dhfr_oak.sdf" );
		Reader reader = new SDFReader();
		
		try {
			Environment env = reader.read( source );
			List<Graph> list = env.getGraphs();
			assertEquals( 25, list.size() );
			
		
		}
		catch( Exception e ) {
			e.printStackTrace();
			fail( e.getMessage() );
		}
	}

}
