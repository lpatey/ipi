package fr.imag.ama.ipi.types;

import java.lang.String;

public class ComposedInstance extends Instance {
	
	private Instance[] values;

	protected ComposedInstance( Instance[] values, ComposedType type) {
		super(type);
		this.values = values;
	}
	
	public Instance[] getValues() {
		return this.values;
	}
	
	public ComposedType getType() {
		return (ComposedType) super.getType();
	}
	
	public String getValueAsString() {
		String ret = "";
		for( Instance i : getValues() ) {
			ret += "," + (i==null ? "?" : i.getValueAsString() );
		}
		return "{" + ("".equals( ret ) ? ret : ret.substring( 1 )) + "}";
	}

	@Override
	public Instance generalization(Instance t)
			throws IncompatibleTypesException {
		
		int length = values.length;
		Instance[] values1 = getValues();
		Instance[] values2 = ((ComposedInstance) t).getValues();
		Instance[] values = new Instance[length];
		
		for( int i=0; i<length; i++ ) {
			values[i] = values1[i].generalization(values2[i]);
		}
		return new ComposedInstance( values, getType());
		
	}

	@Override
	public double inclusion(Instance t) throws IncompatibleTypesException {
			
		int length = values.length;
		Instance[] values1 = getValues();
		Instance[] values2 = ((ComposedInstance) t).getValues();
		double[] values = new double[length];
		for( int i=0; i<length; i++ ) {
			values[i] = values1[i].inclusion(values2[i]);
		}
		return getType().getOperator().compute(values);
	}

	@Override
	public double similarity(Instance t) throws IncompatibleTypesException {
		
		int length = values.length;
		Instance[] values1 = getValues();
		Instance[] values2 = ((ComposedInstance) t).getValues();
		double[] values = new double[length];
		
		for( int i=0; i<length; i++ ) {
			values[i] = values1[i].similarity(values2[i]);
		}
		return getType().getOperator().compute(values);
		
	}

}
