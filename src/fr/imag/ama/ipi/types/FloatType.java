package fr.imag.ama.ipi.types;

import java.lang.String;

import fr.imag.ama.ipi.beans.Environment;
import fr.imag.ama.ipi.tools.ArrayParser;
import fr.imag.ama.ipi.tools.InvalidArrayException;
import fr.imag.ama.ipi.types.Float;

public class FloatType extends NumberType {
	
	private java.lang.Float min = null;
	private java.lang.Float max = null;
	
	public FloatType( String name, String domain, Environment env ) throws InvalidDomainException {
		super( name, domain, env );
		setDomain( domain );
	}
	
	@Override
	public void setDomain(String domain) throws InvalidDomainException {
		super.setDomain(domain);
		try {
			if( domain == null | "".equals( domain ) ) {
				return;
			}
			String[] d = ArrayParser.parse( domain );
			min = java.lang.Float.parseFloat( d[0] );
			max = java.lang.Float.parseFloat( d[1] );
		}
		catch( InvalidArrayException e ) {
			throw new InvalidDomainException( domain, this );
		}
	}

	@Override
	public Float getValue(String o) throws InvalidInstanceException {
		
		if( "?".equals( o.trim() ) || "".equals( o.trim() ) ) {
			return null;
		}
		
		java.lang.Float v = java.lang.Float.parseFloat( o );

		if( v == null || ((getMin() != null) && v < getMin()) || ((getMax() != null) && v > getMax()) ) {
			throw new InvalidInstanceException( o, this );
		}
		
		return new Float( v, this );
	}
	
	public java.lang.Float getMin() {
		return min;
	}
	
	public java.lang.Float getMax() {
		return max;
	}

}
