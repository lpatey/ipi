package fr.imag.ama.ipi.types;

import java.lang.String;

public class InvalidDomainException extends InvalidDataException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidDomainException(String domain, Type type) {
		super( "Domain " + domain + " incompatible with " + type.getName() );
	}

}
