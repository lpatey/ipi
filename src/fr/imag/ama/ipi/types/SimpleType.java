package fr.imag.ama.ipi.types;

import java.lang.String;

import fr.imag.ama.ipi.beans.Environment;


public abstract class SimpleType extends Type {

	private String domain;
	
	public SimpleType( String name, String domain, Environment env ) throws InvalidDomainException {
		super( name, env );
		setDomain( domain );
	}
	
	public void setDomain( String domain )  throws InvalidDomainException {
		this.domain = domain;
	}
	
	public String getDomain() {
		return domain;
	}

}
