package fr.imag.ama.ipi.types;

import java.lang.Double;
import java.lang.String;

public class Interval extends Instance {

	private Double start;
	private Double end;

	public Interval( Double start, Double end, IntervalType type ) {
		super( type );
		this.start = start;
		this.end = end;
	}

	public Double getStart() {
		return start;
	}

	public Double getEnd() {
		return end;
	}

	@Override
	public Instance generalization(Instance t) throws IncompatibleTypesException {
		if( !( t instanceof Interval ) ) {
			throw new IncompatibleTypesException( t.getType(), this.getType() );
		}
		Interval i = (Interval) t;
		try {
			return getType().getValue( "[" +  Math.min( getEnd(), i.getEnd()) + "," + Math.max( getStart(), i.getStart()) + "]" );
		}
		catch( Exception e ) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String getValueAsString() {
		return "[" + getStart() + "," + getEnd() + "]";
	}

	@Override
	public double inclusion(Instance t) throws IncompatibleTypesException {
		if( !( t instanceof Interval ) ) {
			throw new IncompatibleTypesException( t.getType(), this.getType() );
		}
		Interval i = (Interval) t;
		return (Math.min( getEnd(), i.getEnd()) - Math.max( getStart(), i.getStart()))
		/ ( getEnd()-getStart() );
	}

	@Override
	public double similarity(Instance t) throws IncompatibleTypesException {
		if( !( t instanceof Interval ) ) {
			throw new IncompatibleTypesException( t.getType(), this.getType() );
		}
		Interval i = (Interval) t;
		return (Math.min( getEnd(), i.getEnd()) - Math.max( getStart(), i.getStart()))
		/ (Math.max( getEnd(), i.getEnd()) - Math.min( getStart(), i.getStart()));
	}

}
