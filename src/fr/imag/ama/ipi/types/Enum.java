package fr.imag.ama.ipi.types;

import java.lang.String;

public class Enum extends Instance {
	
	int value;
	
	protected Enum( int value, EnumType type ) {
		super( type );
		this.value = value;
	}
	
	public int getValue() {
		return this.value;
	}
	
	public EnumType getType() {
		return (EnumType) super.getType();
	}
	
	public String getName() {
		return getType().getName( this.value );
	}
	
	public String getValueAsString() {
		return getName();
	}

	@Override
	public Instance generalization(Instance t) throws IncompatibleTypesException {
		return this;
	}

	@Override
	public double inclusion(Instance t) throws IncompatibleTypesException {
		return similarity( t );
	}

	@Override
	public double similarity(Instance t) throws IncompatibleTypesException {
		if( !( t instanceof Enum ) ) {
			throw new IncompatibleTypesException( t.getType(), this.getType() );
		}
		Enum e = (Enum) t;
		return e.getValue() == this.getValue() ? 1 : 0;
	}

}
