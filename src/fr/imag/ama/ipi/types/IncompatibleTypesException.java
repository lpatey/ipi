package fr.imag.ama.ipi.types;

public class IncompatibleTypesException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public IncompatibleTypesException( Type typeA, Type typeB ) {
		super( "Incompatible types " + typeA.getName() + " and " + typeB.getName() );
	}

}
