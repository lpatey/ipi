package fr.imag.ama.ipi.types;

import java.lang.String;
import java.util.Arrays;

import fr.imag.ama.ipi.beans.Environment;
import fr.imag.ama.ipi.tools.ArrayParser;
import fr.imag.ama.ipi.tools.InvalidArrayException;

public class EnumType extends SimpleType {

	String[] domain;
	
	public EnumType( String name, String domain, Environment env ) throws InvalidDomainException {
		super( name, domain, env );
		setDomain( domain );
	}
	
	@Override
	public void setDomain(String domain) throws InvalidDomainException {
		super.setDomain(domain);
		try {
			this.domain = ArrayParser.parse( domain );
			Arrays.sort( this.domain );
		}
		catch( InvalidArrayException e ) {
			throw new InvalidDomainException( domain, this);
		}
	}
	
	@Override
	public Enum getValue( String o) throws InvalidInstanceException {
		
		if( "?".equals( o.trim() ) || "".equals( o.trim() ) ) {
			return null;
		}
		
		int value = Arrays.binarySearch( domain, o );
		if( value < 0 ) {
			throw new InvalidInstanceException( o, this );
		}

		return new Enum(value,this);
	}
	
	public String getName( int value ) {
		return this.domain[value];
	}
	
}
