package fr.imag.ama.ipi.types;

import java.lang.String;

public abstract class Instance {
	
	private Type type;
	
	protected Instance( Type type ) {
		this.type = type;
	}
	
	public Type getType() {
		return type;
	}
	
	public abstract String getValueAsString();

	public abstract double inclusion( Instance t ) throws IncompatibleTypesException;
	
	public abstract double similarity( Instance t ) throws IncompatibleTypesException;

	public abstract Instance generalization( Instance t ) throws IncompatibleTypesException;
	
}
