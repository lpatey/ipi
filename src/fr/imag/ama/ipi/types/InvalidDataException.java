package fr.imag.ama.ipi.types;

import java.lang.String;

public abstract class InvalidDataException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public InvalidDataException( String message ) {
		super( message );
	}
}
