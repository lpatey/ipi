package fr.imag.ama.ipi.types;

public class Integer extends Number {
	
	protected Integer( java.lang.Integer value, IntegerType type ) {
		super( value, type );
	}

}
