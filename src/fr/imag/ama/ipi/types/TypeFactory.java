package fr.imag.ama.ipi.types;

import java.lang.String;
import java.lang.reflect.Constructor;

import fr.imag.ama.ipi.beans.Environment;


public class TypeFactory {
	
	private TypeFactory() {
		
	}

	public static TypeFactory getFactory() {
		return new TypeFactory();
	}
	
	public SimpleType newSimpleType( String type, String name, String domain, Environment env ) {
		try {
			
			Class<?> typeClass = Class.forName( this.getClass().getPackage().getName() + "." + type + "Type" );
			
			Constructor<?> c = typeClass.getConstructors()[0];
			return (SimpleType) c.newInstance( new Object[] { name, domain, env } );
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
		
		return null;
	}
	
}
