package fr.imag.ama.ipi.types;

import fr.imag.ama.ipi.beans.Environment;
import fr.imag.ama.ipi.types.String;

public class StringType extends SimpleType {
	
	public StringType( java.lang.String name, java.lang.String domain, Environment env ) throws InvalidDomainException {
		super( name, domain, env );
	}
	
	public StringType( java.lang.String name, Environment env )  throws InvalidDomainException {
		this( name, "", env );
	}

	@Override
	public fr.imag.ama.ipi.types.String getValue(java.lang.String o) throws InvalidInstanceException {
		if( "?".equals( o.trim() ) || "".equals( o.trim() ) ) {
			return null;
		}
		return new String( o, this ) ;
	}

}
