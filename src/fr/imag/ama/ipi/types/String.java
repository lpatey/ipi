package fr.imag.ama.ipi.types;

import fr.imag.ama.ipi.algorithms.LCSAlgorithm;

public class String extends Instance {
	
	private java.lang.String value;
	
	public java.lang.String getValue() {
		return value;
	}
	
	public String( java.lang.String value, StringType type ) {
		super( type );
		this.value = value;
	}
	
	@Override
	public java.lang.String getValueAsString() {
		return "\"" + value + "\"";
	}

	@Override
	public Instance generalization(Instance t) throws IncompatibleTypesException {
		if( !( t instanceof String ) ) {
			throw new IncompatibleTypesException( t.getType(), this.getType() );
		}
		try {
			return getType().getValue( LCSAlgorithm.compute( value, ((String)t).getValue() ) );
		}
		catch( Exception e ) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public double inclusion(Instance t) throws IncompatibleTypesException {
		return similarity(t);
	}

	@Override
	public double similarity(Instance t) throws IncompatibleTypesException {
		if( !( t instanceof String ) ) {
			throw new IncompatibleTypesException( t.getType(), this.getType() );
		}
		
		return ((String)t).getValue().equals(this.getValue()) ? 1 : 0;
		
	}

}
