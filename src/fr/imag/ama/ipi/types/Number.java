package fr.imag.ama.ipi.types;

import java.lang.String;

public abstract class Number extends Instance {

	private java.lang.Number value;

	protected Number( java.lang.Number value, NumberType type ) {
		super( type );
		this.value = value;
	}

	public NumberType getType() {
		return (NumberType) super.getType();
	}

	public java.lang.Number getValue() {
		return value;
	}

	public String getValueAsString() {
		return "" + getValue();
	}

	public double similarity(Instance t) throws IncompatibleTypesException {
		if( !( t instanceof Number ) ) {
			throw new IncompatibleTypesException( t.getType(), this.getType() );
		}
		Number n = (Number) t;
		return 1- (Math.abs(this.getValue().doubleValue() - n.getValue().doubleValue()) 
		/ Math.max( this.getType().getMax().doubleValue(), n.getType().getMax().doubleValue()));
	}

	@Override
	public double inclusion(Instance t) throws IncompatibleTypesException {
		return similarity( t ); 
	}

	@Override
	public Instance generalization(Instance t)
	throws IncompatibleTypesException {
		if( !( t instanceof Number ) ) {
			throw new IncompatibleTypesException( t.getType(), this.getType() );
		}
		Number n = (Number) t;
		double result = (this.getValue().doubleValue() + n.getValue().doubleValue())/2;

		try {
			if( this instanceof Integer && t instanceof Integer ) {
				java.lang.String r = Math.round(result) + "";
				return getType().getValue( r );
			}
			else {
				Instance i = this instanceof Integer ? t : this;
				java.lang.String r = result + "";
				return i.getType().getValue( r );
			}
		}
		catch( Exception e ) {
			e.printStackTrace();
		}

		return null;
	}
}
