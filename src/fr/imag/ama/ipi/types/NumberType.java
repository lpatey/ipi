package fr.imag.ama.ipi.types;

import java.lang.Number;
import java.lang.String;

import fr.imag.ama.ipi.beans.Environment;

public abstract class NumberType extends SimpleType {

	public NumberType( String name, String domain, Environment env )  throws InvalidDomainException {
		super( name, domain, env );
	}
	
	public abstract Number getMin();
	
	public abstract Number getMax();

}
