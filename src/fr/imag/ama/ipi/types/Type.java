package fr.imag.ama.ipi.types;

import java.lang.String;
import java.util.HashMap;

import fr.imag.ama.ipi.beans.Environment;

public abstract class Type {
	
	private static HashMap<String, Type> types = new HashMap<String, Type>();
	
	private String name;
	private Environment environment;
	private int index;
	private HashMap<String, Double> inclusions = new HashMap<String, Double>();
	
	public Type( String name, Environment env ) {
		this.name = name;
		this.index = types.size();
		this.environment = env;
		types.put( name, this );
	}
	
	public Environment getEnvironment() {
		return environment;
	}
	
	public static Type getType( String name ) {
		return types.get( name );
	}

	public static int getIndex( String name ) {
		return types.get( name ).getIndex();
	}
	
	public void addInclusion( String to, double value ) {
		inclusions.put( to, value );
	}
	
	public abstract Instance getValue( String o ) throws InvalidInstanceException;
	
	public double getInclusion( Type t ) {
		return t == this ? 1 : inclusions.get( t );
	}
	
	public HashMap<String, Double> getInclusions() {
		return inclusions;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getIndex() {
		return this.index;
	}
	
}
