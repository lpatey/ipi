package fr.imag.ama.ipi.types;

import java.lang.String;

import fr.imag.ama.ipi.beans.Environment;
import fr.imag.ama.ipi.tools.ArrayParser;
import fr.imag.ama.ipi.tools.InvalidArrayException;
import fr.imag.ama.ipi.tools.Operator;

public class ComposedType extends Type {
	
	private Operator operator;
	private Type[] types;
	private String[] labels;
	
	public ComposedType( String name, String[] labels, Type[] types, Operator operator, Environment env ) {
		super( name, env );
		this.operator = operator;
		this.types = types;
		this.labels = labels;
	}
	
	public String[] getLabels() {
		return labels;
	}
	
	@Override
	public ComposedInstance getValue( String value ) throws InvalidInstanceException {
		
		if( "?".equals( value.trim() ) || "".equals( value.trim() ) ) {
			return null;
		}
		
		try {
			String[] values = ArrayParser.parse( value );
			Instance[] objects = new Instance[ types.length ];
			int length = values.length;
			for( int i=0; i<length; i++ ) {
				objects[i] = types[i].getValue( values[i] );
			}
			return new ComposedInstance( objects, this );
		}
		catch( InvalidArrayException e ) {
			throw new InvalidInstanceException( value, this );
		}
	}
	
	public Operator getOperator() {
		return operator;
	}
	
	public Type[] getTypes() {
		return this.types;
	}

}
