package fr.imag.ama.ipi.types;

import java.lang.String;

import fr.imag.ama.ipi.beans.Environment;
import fr.imag.ama.ipi.tools.ArrayParser;
import fr.imag.ama.ipi.tools.InvalidArrayException;

public class IntegerType extends NumberType {
	
	private java.lang.Integer min;
	private java.lang.Integer max;
	
	public IntegerType( String name, String domain, Environment env ) throws InvalidDomainException {
		super( name, domain, env );
		setDomain( domain );
		
	}
	
	@Override
	public void setDomain(String domain) throws InvalidDomainException {
		super.setDomain(domain);
		try {
			String[] d = ArrayParser.parse( domain );
			min = java.lang.Integer.parseInt( d[0] );
			max = java.lang.Integer.parseInt( d[1] );
		}
		catch( InvalidArrayException e ) {
			throw new InvalidDomainException( domain, this );
		}
	}
	
	@Override
	public Integer getValue( String value ) throws InvalidInstanceException {
		
		if( "?".equals( value.trim() ) || "".equals( value.trim() ) ) {
			return null;
		}
		
		java.lang.Integer v = java.lang.Integer.parseInt( value );
		if( v == null || v < getMin() || v > getMax() ) {
			throw new InvalidInstanceException( value, this );
		}
		return new Integer( v, this );
	}

	public java.lang.Integer getMin() {
		return min;
	}
	
	public java.lang.Integer getMax() {
		return max;
	}
}
