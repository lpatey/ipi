package fr.imag.ama.ipi.types;

import java.lang.String;
import java.lang.Double;

import fr.imag.ama.ipi.beans.Environment;
import fr.imag.ama.ipi.tools.ArrayParser;
import fr.imag.ama.ipi.tools.InvalidArrayException;

public class IntervalType extends SimpleType {
	
	private Double min;
	private Double max;
	
	protected IntervalType( String name, String domain, Environment env ) throws InvalidDomainException {
		super( name, domain, env );
		setDomain( domain );
	}
	
	public Double getMin() {
		return min;
	}
	
	public Double getMax() {
		return max;
	}
	
	@Override
	public void setDomain(String domain) throws InvalidDomainException {
		super.setDomain(domain);
		try {
			String[] d = ArrayParser.parse( domain );
			min = java.lang.Double.parseDouble( d[0] );
			max = java.lang.Double.parseDouble( d[1] );
		}
		catch( InvalidArrayException e ) {
			throw new InvalidDomainException( domain, this );
		}
	}

	@Override
	public Interval getValue(String o) throws InvalidInstanceException {
		
		if( "?".equals( o.trim() ) || "".equals( o.trim() ) ) {
			return null;
		}
		
		try {
			String[] v = ArrayParser.parse( o );
			Double v1 = Double.parseDouble( v[0]);
			Double v2 = Double.parseDouble( v[1] );
			
			if( v1 < getMin() || v2 > getMax() || v1 > v2 ) {
				throw new InvalidInstanceException( o, this );
			}
			return new Interval( v1, v2, this );
		}
		catch( InvalidArrayException e ) {
			throw new InvalidInstanceException( o, this );
		}
	}

}
