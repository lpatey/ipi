package fr.imag.ama.ipi.types;

import java.lang.String;

public class InvalidInstanceException extends InvalidDataException {

	public InvalidInstanceException( String object, Type type ) {
		super( "Data " + object + " incompatible with " + type.getName() );
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	
}
