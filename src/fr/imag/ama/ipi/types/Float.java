package fr.imag.ama.ipi.types;

public class Float extends Number {

	protected Float( java.lang.Float value, FloatType type ) {
		super( value, type );
	}
}
