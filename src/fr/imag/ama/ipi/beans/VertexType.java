package fr.imag.ama.ipi.beans;

import fr.imag.ama.ipi.tools.ArrayParser;
import fr.imag.ama.ipi.tools.Operator;
import fr.imag.ama.ipi.types.ComposedType;
import fr.imag.ama.ipi.types.Instance;
import fr.imag.ama.ipi.types.InvalidInstanceException;
import fr.imag.ama.ipi.types.Type;

public class VertexType extends ComposedType {
	
	public VertexType( String name, String[] labels, Type[] types, Operator operator, Environment env ) {
		super( name, labels, types, operator, env );
	}
	
	public Vertex getValue( String name, Graph graph, int index, String value ) throws InvalidInstanceException {
		
		Type[] types = getTypes();
		try {
			String[] values = ArrayParser.parse( value );
			Instance[] objects = new Instance[ types.length ];
			int length = values.length;
			for( int i=0; i<length; i++ ) {
				objects[i] = types[i].getValue( values[i] );
			}
			return new Vertex( name, graph, index, objects, this );
		}
		catch( Exception e ) {
			throw new InvalidInstanceException( value, this );
		}
	}

}
