package fr.imag.ama.ipi.beans;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import fr.imag.ama.ipi.types.ComposedInstance;
import fr.imag.ama.ipi.types.Instance;

/**
 * Represents a complex object which contains entities and literals
 * @author The AMA Team
 *
 * @param <V> The vertex class
 * @param <E> The edge class
 * @param <G> The graph class
 */
public class Graph extends ComposedInstance {
	
	private String name;
	private Vertex[] vertices;
	private Edge[] edges;
	private int index;
	
	private HashMap<String, Vertex> verticesByName = new HashMap<String, Vertex>();

	public Graph( String name, int index, Instance[] values, GraphType type ) {
		super( values, type );
		this.name = name;
		this.index = index;
		
		// Array initialization
		this.edges = new Edge[0];
		this.vertices = new Vertex[0];
	}
	
	public final GraphType getType() {
		return (GraphType) super.getType();
	}

	public final String getName() {
		return name;
	}

	public final int getIndex() {
		return index;
	}
	
	/**
	 * Returns the whole list of entities
	 * @return An entities' list
	 */
	public final Vertex[] getVertices() {
		return vertices;
	}

	/**
	 * Returns the whole list of literals
	 * @return A literals' list
	 */
	public final Edge[] getEdges() {
		return edges;
	}
	
	public final Vertex getVertexByName( String name ) {
		return verticesByName.get( name );
	}

	/**
	 * Adds an entity 
	 * @param vertex The entity to add
	 */
	public final void add(Vertex vertex) {
		List<Vertex> list = new ArrayList<Vertex>( Arrays.asList( this.vertices ) );
		list.add( vertex );
		this.vertices = list.toArray( new Vertex[ list.size() ] );
		verticesByName.put( vertex.getName(), vertex );
	}

	/**
	 * Adds a literal
	 * @param edge The literal to add
	 */
	public final void add(Edge edge) {
		List<Edge> list = new ArrayList<Edge>( Arrays.asList( this.edges ) );
		list.add( edge );
		this.edges = list.toArray( new Edge[list.size()] );
	}
	
	/**
	 * Returns a string representation of the object
	 */
	public String toString() {
		
		String ret = name;
		ret += " [" + getValueAsString() + "]";
		ret+= " (";
		for( Vertex vertex : getVertices() ) {
			ret += vertex.getValueAsString() + "-";
		}
		ret = ret.substring(0,ret.length()-1) + ") ";
		
		ret+= " {";
		for( Edge edge : getEdges() ) {
			ret += edge.getValueAsString() + ",";
		}
		ret = ret.substring(0,ret.length()-1) + "}";
		
		return ret;
		
	}
	
	public void computeOptimizations() {

		for( Vertex v : getVertices() ) {
			v.computeOptimizations();
		}
		
		for( Edge e : getEdges() ) {
			e.computeOptimizations();
		}
	
	}
	

}
