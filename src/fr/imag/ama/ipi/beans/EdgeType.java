package fr.imag.ama.ipi.beans;

import fr.imag.ama.ipi.tools.ArrayParser;
import fr.imag.ama.ipi.tools.InvalidArrayException;
import fr.imag.ama.ipi.tools.Operator;
import fr.imag.ama.ipi.types.ComposedType;
import fr.imag.ama.ipi.types.Instance;
import fr.imag.ama.ipi.types.InvalidInstanceException;
import fr.imag.ama.ipi.types.Type;

public class EdgeType extends ComposedType {
	
	private String[] verticesLabels;
	private boolean directed;
	private VertexType[] verticesTypes;
	
	public EdgeType( String name, String[] labels, Type[] types, VertexType[] verticesTypes, Operator operator, boolean directed, Environment env ) {
		this( name, labels, types, null, verticesTypes, operator, directed, env );
	}
	
	public EdgeType( String name, String[] labels, Type[] types, String[] verticesLabels, VertexType[] verticesTypes, Operator operator, boolean directed, Environment env ) {
		super( name, labels, types, operator, env );
		this.verticesLabels = verticesLabels;
		this.verticesTypes = verticesTypes;
		this.directed = directed;
	}
	
	public boolean isDirected() {
		return directed;
	}
	
	public VertexType[] getVerticesTypes() {
		return verticesTypes;
	}
	
	public String[] getVerticesLabels() {
		return verticesLabels;
	}
	
	public Edge getValue( Graph graph, int index, Vertex[] vertices, String value ) throws InvalidInstanceException {
		return getValue( "", graph, index, vertices, value );
	}
	
	public Edge getValue( String name, Graph graph, int index, Vertex[] vertices, String value ) throws InvalidInstanceException {
		
		Type[] types = getTypes();
		try {
			String[] values = ArrayParser.parse( value );
			Instance[] objects = new Instance[ types.length ];
			int length = values.length;
			for( int i=0; i<length; i++ ) {
				objects[i] = types[i].getValue( values[i] );
			}
			return new Edge( name, graph, index, vertices, objects, this );
		}
		catch( InvalidArrayException e ) {
			throw new InvalidInstanceException( value, this );
		}
	}

}
