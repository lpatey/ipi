package fr.imag.ama.ipi.beans;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import fr.imag.ama.ipi.types.ComposedType;
import fr.imag.ama.ipi.types.Type;

public class Environment {
	
	private HashMap<String, String> headers = new HashMap<String, String>();
	
	private ArrayList<Graph> graphs = new ArrayList<Graph>();
	
	
	private ArrayList<Type> macrosList = new ArrayList<Type>();

	private HashMap<String,Type> macros = new HashMap<String,Type>();
	private HashMap<String,GraphType> graphsTypes = new HashMap<String,GraphType>();
	private HashMap<String,VertexType> verticesTypes = new HashMap<String,VertexType>();
	private HashMap<String,EdgeType> edgesTypes = new HashMap<String,EdgeType>();
	
	public Environment() {
		headers.put( "default-similarity-operator", "Average" );
	}
	
	public void inherit( Environment env ) {
		
		if( env == null ) {
			return;
		}
		
		headers = new HashMap<String, String>( env.headers );
		macros = new HashMap<String, Type>( env.macros );
		graphsTypes = new HashMap<String, GraphType>( env.graphsTypes );
		verticesTypes = new HashMap<String, VertexType>( env.verticesTypes );
		edgesTypes = new HashMap<String, EdgeType>( env.edgesTypes );
		
	}

	public ArrayList<Graph> getGraphs() {
		return graphs;
	}
	
	public GraphType getGraphType( String name ) {
		return graphsTypes.get( name );
	}
	
	public EdgeType getEdgeType( String name ) {
		return edgesTypes.get( name );
	}
	
	public VertexType getVertexType( String name ) {
		return verticesTypes.get( name );
	}
	
	public Type getMacro( String name ) {
		return macros.get( name );
	}
	
	public String getDefaultSimilarityOperator() {
		return headers.containsKey( "default-similarity-operator" ) ? 
				headers.get( "default-similarity-operator" ) : "average";
	}
	
	public boolean isVerticesBasedMatching() {
		return Boolean.parseBoolean(headers.containsKey( "vertices-based-matchingr" ) ? 
				headers.get( "vertices-based-matching" ) : "true" );
	}

	public Collection<Type> getMacros() {
		return macrosList;
	}
	
	public Collection<GraphType> getGraphTypes() {
		return graphsTypes.values();
	}

	public Collection<VertexType> getVerticesTypes() {
		return verticesTypes.values();
	}

	public Collection<EdgeType> getEdgesTypes() {
		return edgesTypes.values();
	}
	
	public HashMap<String, String> getHeaders() {
		return headers;
	}

	
	public void addHeader( String name, String value ) {
		headers.put( name, value );
	}
	
	public void addGraph( Graph g ) {
		if( !graphsTypes.containsKey( g.getType().getName() ) ) {
			addGraphType( g.getType() );
		}
		for( Vertex v : g.getVertices() ) {
			if( !verticesTypes.containsKey( v.getType().getName() ) ) {
				addVertexType( v.getType() );
			}
		}
		for( Edge e : g.getEdges() ) {
			if( !edgesTypes.containsKey( e.getType().getName() ) ) {
				addEdgeType( e.getType() );
			}
		}
		this.graphs.add( g );
	}

	public void addMacro( Type t ) {
		if( t instanceof ComposedType ) {
			ComposedType type = (ComposedType) t;
			for( Type childType : type.getTypes() ) {
				if( !this.macros.containsKey( childType.getName() ) ) {
					addMacro( childType );
				}
			}
		}
		this.macros.put( t.getName(), t );
		this.macrosList.add( t );
	}
	
	public void addGraphType( GraphType gt ) {
		for( Type childType : gt.getTypes() ) {
			if( !this.macros.containsKey( childType.getName() ) ) {
				addMacro( childType );
			}
		}
		this.graphsTypes.put( gt.getName(), gt );
	}
	
	public void addVertexType( VertexType vt ) {
		for( Type childType : vt.getTypes() ) {
			if( !this.macros.containsKey( childType.getName() ) ) {
				addMacro( childType );
			}
		}
		this.verticesTypes.put( vt.getName(), vt );
	}
	
	public void addEdgeType( EdgeType et ) {
		for( Type childType : et.getTypes() ) {
			if( !this.macros.containsKey( childType.getName() ) ) {
				addMacro( childType );
			}
		}
		this.edgesTypes.put( et.getName(), et );
	}

}
