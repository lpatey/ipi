package fr.imag.ama.ipi.beans;

import java.util.HashMap;

import fr.imag.ama.ipi.tools.ArrayParser;
import fr.imag.ama.ipi.tools.InvalidArrayException;
import fr.imag.ama.ipi.tools.Operator;
import fr.imag.ama.ipi.types.ComposedType;
import fr.imag.ama.ipi.types.Instance;
import fr.imag.ama.ipi.types.InvalidInstanceException;
import fr.imag.ama.ipi.types.Type;

public class GraphType extends ComposedType {

	public GraphType( String name, String[] labels, Type[] types, Operator operator, Environment env ) {
		super( name, labels, types, operator, env );
	}
	
	public Graph getValue( String name, int index, String value ) throws InvalidInstanceException {
		
		Type[] types = getTypes();
		try {
			String[] values = ArrayParser.parse( value );
			Instance[] objects = new Instance[ types.length ];
			int length = values.length;
			for( int i=0; i<length; i++ ) {
				objects[i] = types[i].getValue( values[i] );
			}
			return new Graph( name, index, objects, this );
		}
		catch( InvalidArrayException e ) {
			throw new InvalidInstanceException( value, this );
		}
	}
	
	public Graph getValue( String name, int index, HashMap<String, String> values ) throws InvalidInstanceException {
		
		Type[] types = getTypes();
		
		Instance[] objects = new Instance[ types.length ];
		for( int i=0; i<types.length; i++ ) {
			if( values.containsKey( types[i].getName() ) ) {
				objects[i] = types[i].getValue( values.get( types[i].getName() ) );
			}
			else {
				objects[i] = types[i].getValue( "?" );
			}
		}
		return new Graph( name, index, objects, this );
		
	}
	
}
