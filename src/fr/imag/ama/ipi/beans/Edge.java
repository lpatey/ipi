package fr.imag.ama.ipi.beans;

import fr.imag.ama.ipi.types.ComposedInstance;
import fr.imag.ama.ipi.types.ComposedType;
import fr.imag.ama.ipi.types.Instance;


/**
 * Represents an association between two vertices, inside a graph
 * @author The AMA Team
 *
 * @param <V> The vertex class
 * @param <V> The edge class
 * @param <G> The graph class
 */
public class Edge extends ComposedInstance implements Comparable<Edge> {
	
	private Vertex[] vertices;
	private Graph graph;
	private int arity = 0;
	private String name;
	private int index;

	/**
	 * Instanciates the edge with its owner
	 * @param graph The edge's owner
	 */
	public Edge( String name, Graph graph, int index, Vertex[] vertices, Instance[] values, ComposedType type ) {
		super( values, type );
		this.graph = graph;
		this.vertices = vertices;
		this.arity = vertices.length;
		this.name = name;
		this.index = index;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getIndex() {
		return this.index;
	}
	
	public String getVerticesAsString() {
		String ret = "";
		for( Vertex v : getVertices() ) {
			ret += "," + v.getName();
		}
		return "{" + ret.substring( 1 ) + "}";
	}
	
	@Override
	public EdgeType getType() {
		return (EdgeType) super.getType();
	}
	
	/**
	 * Returns the graph owner of the edge
	 * @return The owner
	 */
	public final Graph getGraph() {
		return graph;
	}
	
	public final boolean isDirected() {
		return getType().isDirected();
	}
	
	/**
	 * Returns the vertices associated with this edge
	 * @return A list of vertices
	 */
	public final Vertex[] getVertices() {
		return vertices;
	}
	
	public final int getArity() {
		return this.arity;
	}


	public int compareTo(Edge o) {
		return getClass().getName().hashCode() - o.getClass().getName().hashCode();
	}
	
	/**
	 * Returns a string representation of the object
	 */
	public String toString() {
		String ret = "(";
		for( Vertex vertex : getVertices() ) {
			ret += vertex + ",";
			
		}
		return ret.substring(0,ret.length()-1) + ")";
	}

	public void computeOptimizations() {
	
	}
}
