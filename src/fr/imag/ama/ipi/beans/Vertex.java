package fr.imag.ama.ipi.beans;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import fr.imag.ama.ipi.types.ComposedInstance;
import fr.imag.ama.ipi.types.Instance;

/**
 * 
 * Represents the smallest component of the instance representation
 * @author The AMA Team
 *
 * @param <V> The vertex class
 * @param <E> The edge class
 * @param <G> The graph class
 */
public class Vertex extends ComposedInstance {
	
	private Graph graph;
	private int index;
	private Edge[] edges;
	private Vertex[] neighbors;
	private Edge[][] edgesFromVertex;
	private String name;
	
	public Vertex( String name, Graph graph, int index, Instance[] values, VertexType type ) {
		super( values, type );
		this.graph = graph;
		this.index = index;
		this.edges = new Edge[0];
		this.name = name;
	}
	
	public final String getName() {
		return this.name;
	}
	
	public final void addEdge( Edge edge ) {
		List<Edge> list = new ArrayList<Edge>( Arrays.asList( this.edges ) );
		list.add( edge );
		this.edges = list.toArray( (Edge[]) Array.newInstance( edge.getClass(), list.size() ) );
	}
	
	@Override
	public final VertexType getType() {
		return (VertexType) super.getType();
	}
	
	public final Graph getGraph() {
		return graph;
	}
	
	public final int getIndex() {
		return index;
	}
	
	public final Edge[] getEdges() {
		return edges;
	}
	
	public final Edge[] getEdges( Vertex vertex ) {
		return edgesFromVertex[ vertex.getIndex() ];
	}
	
	public final Vertex[] getNeighbors() {
		return neighbors;
	}
	
	public final List<Vertex> getMatchableNeighbors( Edge[] edgesB, Vertex vertex ) {
		
		List<Vertex> neighbors = new ArrayList<Vertex>();
		
		for( Vertex neighbor : getNeighbors() ) {

			try {
				if( neighbor.getType().getInclusion( vertex.getType() ) != 0
						&& neighbor.similarity( vertex ) != 0 ) {
					neighbors.add( neighbor );
				}
			}
			catch( Exception e ) {
				e.printStackTrace();
			}
			
		}

		return neighbors;
	}

	public void computeOptimizations() {
		
		// Compute neighbors
		ArrayList<Vertex> list = new ArrayList<Vertex>();
		for( Edge edge : getEdges() ) {
			for( Vertex vertex : edge.getVertices() )
			list.add( vertex );
		}
		list.remove(this);
		neighbors = list.toArray( new Vertex[ list.size() ] );
		
		
		int verticesLength = getGraph().getVertices().length;
		// Compute edges by vertex index
		edgesFromVertex = new Edge[verticesLength][];
		for( Vertex v : getNeighbors() ) {
			ArrayList<Edge> edges = new ArrayList<Edge>();
			for( Edge e : v.getEdges() ) {
				for( Vertex v2 : e.getVertices() ) {
					if( v2 == this ) {
						edges.add( e );
					}
				}
			}
			Collections.sort( edges );
			edgesFromVertex[v.getIndex()] = edges.toArray( new Edge[ edges.size() ] );
		}
		
	}
}
