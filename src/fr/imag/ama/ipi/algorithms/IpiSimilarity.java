package fr.imag.ama.ipi.algorithms;

import java.util.Arrays;

import fr.imag.ama.ipi.algorithms.families.GraphMatchingAlgorithm;
import fr.imag.ama.ipi.algorithms.families.IpiSumTool;
import fr.imag.ama.ipi.algorithms.families.MaximalCostAlgorithm;
import fr.imag.ama.ipi.algorithms.families.SimilarityAlgorithm;
import fr.imag.ama.ipi.beans.Edge;
import fr.imag.ama.ipi.beans.Graph;
import fr.imag.ama.ipi.beans.Vertex;
import fr.imag.ama.ipi.types.IncompatibleTypesException;


public class IpiSimilarity implements SimilarityAlgorithm {

	private MaximalCostAlgorithm localAlgorithm;
	private GraphMatchingAlgorithm globalAlgorithm;
	private IpiSumTool sumTool;
	private float VARIATIONS_THRESHOLD = 0.001f;
	public static boolean get = false;

	public IpiSimilarity( MaximalCostAlgorithm localAlgorithm, 
			GraphMatchingAlgorithm globalAlgorithm, 
			IpiSumTool sumTool,
			float VARIATIONS_THRESHOLD ) {
		this.localAlgorithm = localAlgorithm;
		this.globalAlgorithm = globalAlgorithm;
		this.sumTool = sumTool;
		this.VARIATIONS_THRESHOLD = VARIATIONS_THRESHOLD;
	}

	public IpiSimilarity( MaximalCostAlgorithm localAlgorithm, 
			GraphMatchingAlgorithm globalAlgorithm ) {
		this( localAlgorithm, globalAlgorithm, new DefaultIpiSumTool(), 0.001f );
	}

	public IpiSimilarity( MaximalCostAlgorithm localAlgorithm, 
			GraphMatchingAlgorithm globalAlgorithm, float threshold ) {
		this( localAlgorithm, globalAlgorithm, new DefaultIpiSumTool(), threshold );
	}

	public final double getSimilarityDegree( Graph a, Graph b ) {
		return (getInclusionDegree( a, b )
				+ getInclusionDegree( b, a ) )/2;
	}

	public final double getInclusionDegree( Graph a, Graph b ) {

		double inclusionDegree = 0;

		/**************************************
		 * Local similarity
		 **************************************/
		double[][] subsumptionMatrixAB = getSubsumptionMatrix(a, b);


		/**************************************
		 * Global similarity
		 **************************************/
		inclusionDegree = globalAlgorithm.compute( a, b, subsumptionMatrixAB );

		return inclusionDegree;

	}

	/**
	 * Computes subsumption matrix with Ipi similarity algorithm
	 * Corresponds to the local similarity
	 * @param a First graph
	 * @param b Second graph
	 * @param nIterations Number of Jacobi's iterations. ( Use 5 )
	 * @return The subsumption matrix
	 */
	public final double[][] getSubsumptionMatrix( Graph a, Graph b ) {

		int nLines = a.getVertices().length;
		int nColumns = b.getVertices().length;
		double THRESHOLD = this.VARIATIONS_THRESHOLD;

		double[][] sub = new double[ nLines ][ nColumns ];

		// Initialize the subsumption matrix with 1
		for( int i=0; i<nLines; i++ ) {
			Arrays.fill(sub[i], 1.0 );
		}

		double variation = Double.MAX_VALUE;
		//IpiSimilarityTest.N_ITEMS++;

		int nEdgesA = a.getEdges().length;
		int nEdgesB = b.getEdges().length;

		double[][] verticesInclusions = new double[ a.getVertices().length][ b.getVertices().length ];
		double[][] edgesInclusions = new double[ nEdgesA ][ nEdgesB ];
		boolean[][] edgesComputed = new boolean[ nEdgesA ][ nEdgesB ];

		// Jacobi iterations
		int i=0;
		while( variation > THRESHOLD ) {// Jacobi iterations

			variation = 0;
			//IpiSimilarityTest.N_ITERATIONS++;

			// For each pair of vertices of respectively A and B
			for( Vertex vertexA : a.getVertices() ) {
				int indexA = vertexA.getIndex();
				double[] subVector = sub[indexA];
				double[] inclusionsVector = verticesInclusions[indexA];
				for( Vertex vertexB : b.getVertices() ) {
					
					int indexB = vertexB.getIndex();
				
					if( i==0 ) {
						double inclusionType = vertexA.getType().getInclusion( vertexB.getType() );

						if( inclusionType == 0 ) {
							continue;
						}

						try {
							inclusionsVector[indexB] = inclusionType*vertexA.inclusion(vertexB);
						}
						catch( IncompatibleTypesException e ) {
							e.printStackTrace();
						}
					}
					else if( inclusionsVector[indexB] == 0 ) {
						continue;
					}


					double mapping = 0;

					if( vertexA.getType().getEnvironment().isVerticesBasedMatching() ) {
						mapping = findMaxNeighborsMapping(sub, vertexA, vertexB, edgesInclusions, edgesComputed );
					}
					else {
						mapping = findMaxEdgesMapping(sub, vertexA, vertexB, edgesInclusions, edgesComputed );
					}
				
					double previous = subVector[indexB];
					subVector[indexB] = sumTool.computeVertexAndEdgeSum(inclusionsVector[indexB], mapping);

					variation = Math.max( variation, Math.abs(subVector[indexB] - previous) );
				}
			}
			
			i++;
		}

		return sub;
	}


	/**
	 * Computes coefficient of maximal mapping between two vertices by using an edge mapping
	 * Uses hard coded sum if edges.length < 5
	 * either, uses Hungarian algorithm
	 * @param sub The iterating subsumption matrix
	 * @param vertexA The first vertex
	 * @param vertexB The second vertex
	 * @return
	 */
	private final double findMaxEdgesMapping( double[][] sub, Vertex vertexA, Vertex vertexB, double[][] edgesInclusions, boolean[][] edgesComputed ) {

		// Creates a local matrix
		double[][] localMatrix = computeLocalEdgesMatrix( sub, vertexA, vertexB, edgesInclusions, edgesComputed );

		return localAlgorithm.compute( localMatrix ) / vertexA.getEdges().length;

	}

	/**
	 * Computes coefficient of maximal mapping between two vertices by using an edge mapping
	 * Uses hard coded sum if edges.length < 5
	 * either, uses Hungarian algorithm
	 * @param sub The iterating subsumption matrix
	 * @param vertexA The first vertex
	 * @param vertexB The second vertex
	 * @return
	 */
	private final double findMaxNeighborsMapping( double[][] sub, Vertex vertexA, Vertex vertexB, double[][] edgesInclusions, boolean[][] edgesComputed ) {

		// Creates a local matrix
		double[][] localMatrix = computeLocalNeighborsMatrix( sub, vertexA, vertexB, edgesInclusions, edgesComputed );

		return localAlgorithm.compute( localMatrix ) / vertexA.getNeighbors().length;

	}


	/**
	 * Computes a similarity matrix between the edges of two vertices
	 * @param matrix The local matrix
	 * @param costs The costs matrix : costs[i][j] = (1-matrix[i][j])*1000000
	 * @param sub The subsumption matrix
	 * @param vertexA The first entity
	 * @param vertexB The second entity
	 */
	private final double[][] computeLocalEdgesMatrix( 
			double[][] sub, Vertex vertexA, Vertex vertexB, double[][] edgesInclusions, boolean[][] edgesComputed ) {

		Edge[] edgesA = vertexA.getEdges();
		Edge[] edgesB = vertexB.getEdges();
		int sizeA = edgesA.length;
		int sizeB = edgesB.length;
		double[][] matrix = new double[ sizeA ][ sizeB ];

		Vertex[] verticesA, verticesB;

		// For each pair of edges of respectively A and B
		for( int i=0; i<sizeA; i++ ) {
			Edge edgeA = edgesA[ i ];
			verticesA = edgeA.getVertices();
			int length = verticesA.length;
			int indexA = edgeA.getIndex();

			double[] edgesInclusionsVector = edgesInclusions[indexA];
			boolean[] edgesComputedVector = edgesComputed[indexA];

			for( int j=0; j<sizeB; j++ ) {
				Edge edgeB = edgesB[ j ];

				int indexB = edgeB.getIndex();

				if( !edgesComputedVector[indexB] ) {
					edgesComputedVector[indexB] = true;
					double inclusionType = edgeA.getType().getInclusion( edgeB.getType() );
					if( inclusionType == 0 ) {
						continue;
					}

					try {
						edgesInclusionsVector[indexB] = inclusionType*edgeA.similarity(edgeB);
					}
					catch( IncompatibleTypesException e ) {
						e.printStackTrace();
					}


				}

				if( edgesInclusionsVector[indexB] == 0 ) {
					continue;
				}

				double sum = 0;

				verticesB = edgeB.getVertices();

				// edgeA.getClass() == edgeB.getClass()
				if( edgeA.isDirected() ) {

					// Don't compare edges if position is different
					if( Arrays.binarySearch(verticesA, vertexA)
							!= Arrays.binarySearch( verticesB, vertexB) ) {
						continue;
					}

					// Compare vertices at the same position
					for( int k=0; k<length; k++ ) {
						sum += sub[verticesA[k].getIndex()][verticesB[k].getIndex()];
					}

				}
				else {

					// Raw computation for graphs ( by opposition to hypergraphs )
					if( length == 2 ) {

						Vertex neighborA = verticesA[ verticesA[0] == vertexA ? 1 : 0 ];
						Vertex neighborB = verticesB[ verticesB[0] == vertexB ? 1 : 0 ];

						matrix[i][j] = sumTool.computeEdgeAndNeighboursSum(
								edgesInclusionsVector[indexB], 
								sub[ neighborA.getIndex() ][ neighborB.getIndex()]);

						continue;
					}

					// In this part, it is an hypergraph

					// Create a matrix for assignment problem
					double[][] matrix2 = new double[ length - 1 ][ length - 1];

					int diffA = 0;
					int diffB = 0;
					for( int k=0; k<length; k++ ) {
						Vertex neighborA = verticesA[k];
						if( neighborA == vertexA ) {
							diffA = 1;
							continue;
						}
						for( int l=0; l<length; l++ ) {
							Vertex neighborB = verticesB[l];
							if( neighborB == vertexB ) {
								diffB = 1;
								continue;
							}
							matrix2[ k-diffA ][ l-diffB ] = sub[ neighborA.getIndex() ][ neighborB.getIndex() ];
						}
					}

					// Solve it through an assignment algorithm
					sum = localAlgorithm.compute(matrix2);

				}


				matrix[i][j] = sumTool.computeEdgeAndNeighboursSum(edgesInclusionsVector[indexB], sum/(length-1) );

			}

		}

		return matrix;
	}

	/**
	 * Computes a similarity matrix between the neighbors of two edges
	 * @param matrix The local matrix
	 * @param costs The costs matrix : costs[i][j] = (1-matrix[i][j])*1000000
	 * @param sub The subsumption matrix
	 * @param vertexA The first entity
	 * @param vertexB The second entity
	 */
	private final double[][] computeLocalNeighborsMatrix( 
			double[][] sub, Vertex vertexA, Vertex vertexB, double[][] edgesInclusions, boolean[][] edgesComputed ) {

		Vertex[] neighborsA = vertexA.getNeighbors();
		Vertex[] neighborsB = vertexB.getNeighbors();
		int sizeA = neighborsA.length;
		int sizeB = neighborsB.length;
		int size = Math.max( sizeA, sizeB );
		double[][] matrix = new double[ size ][ size ];

		// For each couple of vertices neighbors of respectively A and B
		for( int i=0; i<sizeA; i++ ) {
			Vertex neighborA = neighborsA[ i ];
			Edge[] edgesA = neighborA.getEdges( vertexA );
			int lengthA = edgesA.length;

			int indexA = neighborA.getIndex();
			double[] subA = sub[ indexA ];

			for( int j=0; j<sizeB; j++ ) {
				Vertex neighborB = neighborsB[ j ];
				Edge[] edgesB = neighborB.getEdges( vertexB );
				int lengthB = edgesB.length;
				int indexB = neighborB.getIndex();
				double sim = 0;

			
				int m=0;
				// For each couple of edges
				for( int k=0; k<lengthA; k++ ) {
					Edge edgeA = edgesA[k];
					int indexEdgeA = edgeA.getIndex();
					double[] edgesInclusionsVector = edgesInclusions[indexEdgeA];
					boolean[] edgesComputedVector = edgesComputed[indexEdgeA];
					for( int l=m; l<lengthB; l++ ) {

						Edge edgeB = edgesB[l];
						int indexEdgeB = edgeB.getIndex();

						if( !edgesComputedVector[ indexEdgeB] ) {
							edgesComputedVector[ indexEdgeB] = true;
							double inclusionType = edgeA.getType().getInclusion( edgeB.getType() );

							if( inclusionType == 0 ) {
								continue;
							}
							try {
								edgesInclusionsVector[ indexEdgeB ] = inclusionType*edgeA.inclusion(edgeB);
							}
							catch( IncompatibleTypesException e ) {
								e.printStackTrace();
							}
						}


						if( edgesInclusionsVector[ indexEdgeB ] == 0 ) {
							continue;
						}

						m=l+1;

						sim += edgesInclusionsVector[ indexEdgeB ];

						break;
					}
				}

				double simEdges = sim/lengthA;
				double simNeighbor = subA[ indexB ];
				matrix[i][j] = sumTool.computeEdgeAndNeighboursSum(simEdges, simNeighbor );
			}

		}

		return matrix;
	}

}
