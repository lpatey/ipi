package fr.imag.ama.ipi.algorithms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;


import fr.imag.ama.ipi.algorithms.families.GraphMatchingAlgorithm;
import fr.imag.ama.ipi.beans.Graph;
import fr.imag.ama.ipi.beans.Vertex;
import fr.imag.ama.ipi.tools.GreedyAlgorithmSeed;
import fr.imag.ama.ipi.tools.Matching;
import fr.imag.ama.ipi.tools.Pair;

public class GreedyAlgorithm implements GraphMatchingAlgorithm {
	
	
	private Random random = new Random( new Date().getTime() );
	private int threshold = 10;
	
	public GreedyAlgorithm() {
		this( 10 );
	}
	
	public GreedyAlgorithm( int threshold ) {
		this.threshold = threshold;
	}

	public final double compute( Graph a, Graph b, double[][] subsumptionMatrix ) {
	
		ArrayList<Pair<Vertex, Vertex>> verticesToExplore = new ArrayList<Pair<Vertex,Vertex>>();
		boolean[][] previousMatchings = new boolean[a.getVertices().length][b.getVertices().length];
		Matching sigma = new Matching( a.getVertices() );
		double maxIpi = 0;
		int i=0;

		for( GreedyAlgorithmSeed seed : getSeeds(a, b, subsumptionMatrix ) ) {
		
			if( previousMatchings[seed.getVertexA().getIndex()][seed.getVertexB().getIndex()] ) {
				continue;
			}

			if( (++i > threshold) && (threshold > 0) ) {
				break;
			}
			Vertex vertexA = seed.getVertexA();
			Vertex vertexB = seed.getVertexB();
			
			sigma = breadthFirstMatching(vertexA, vertexB, sigma, previousMatchings, subsumptionMatrix, verticesToExplore);
		
			double value = sigma.getSubsumptionIndex();
			
			maxIpi = Math.max( maxIpi, value );
			
		}

		return maxIpi;
	}
	
	/**
	 * Returns a list of seeds of two instances, ordered by descending subsumption index
	 * @param a The first instance
	 * @param b The second instance
	 * @param subsumptionMatrix The subsumption matrix calculated by the IpiSimilarityAlgorithm class
	 * @return The seed list
	 */
	private final ArrayList<GreedyAlgorithmSeed> getSeeds( Graph a, Graph b, double[][] subsumptionMatrix ) {
		ArrayList<GreedyAlgorithmSeed> seeds = new ArrayList<GreedyAlgorithmSeed>();

		for( Vertex vertexA : a.getVertices() ) {
			double[] subsumptionMatrixA = subsumptionMatrix[ vertexA.getIndex() ];
			for( Vertex vertexB : b.getVertices() ) {
				double subsumption = subsumptionMatrixA[ vertexB.getIndex() ];
				if( subsumption > 0 ) {
					seeds.add( new GreedyAlgorithmSeed( vertexA, vertexB, subsumption ) );
				}
			}
		}
		
		Collections.sort( seeds );
		return seeds;
	}

	private final Matching breadthFirstMatching( Vertex vertexA, Vertex vertexB, Matching sigma, boolean[][] previousMatchings, double[][] subsumptionMatrix, ArrayList<Pair<Vertex, Vertex>> verticesToExplore ) {
	
		sigma.clear();
		
		for( Vertex neighbor : vertexA.getNeighbors() ) {
				verticesToExplore.add( new Pair<Vertex, Vertex>( neighbor, vertexA ) );
		}
		
		sigma.setSigma( vertexA, vertexB);
		previousMatchings[vertexA.getIndex()][vertexB.getIndex()] = true;
		
		
		while( !verticesToExplore.isEmpty() ) {
			Pair<Vertex, Vertex> pair = verticesToExplore.remove(0);
			Vertex childA = pair.getFirst();
			Vertex fatherA = pair.getSecond();
			Vertex fatherB = sigma.getSigma( fatherA );
			
			if( sigma.hasMatching( childA ) ) {
				continue;
			}
		
			List<Vertex> matchableNeighborsB = fatherB.getMatchableNeighbors(
					fatherA.getEdges( childA ), childA);
			
			
			Vertex selection = bestMatchingVertex( sigma, childA, matchableNeighborsB, subsumptionMatrix );
			
			if( selection == null ) {
				continue;
			}

			sigma.setSigma( childA, selection );
			previousMatchings[childA.getIndex()][selection.getIndex()] = true;
			
			Vertex[] neighborsA = childA.getNeighbors();
			Collections.shuffle( Arrays.asList( neighborsA ), random );
			for( Vertex neighborA : neighborsA ) {
				if( !sigma.hasMatching(neighborA ) ) {
					verticesToExplore.add( new Pair<Vertex,Vertex>( neighborA, childA ));
				}
			}
			
			
		}
		
		return sigma;
	}
	
	private final Vertex bestMatchingVertex( Matching sigma, Vertex childA, List<Vertex> matchableNeighborsB, double[][] subsumptionMatrix ) {

		Vertex selectionB = null;
		double subsumption = -1;
		
		for( Vertex neighborB : matchableNeighborsB ) {
			if( !sigma.hasMatcher(neighborB)) {
				if( subsumptionMatrix[childA.getIndex()][neighborB.getIndex()] > subsumption ) {
					subsumption = subsumptionMatrix[childA.getIndex()][neighborB.getIndex()];
					selectionB = neighborB;
				}
			}
		}

		return selectionB;
	}
}
