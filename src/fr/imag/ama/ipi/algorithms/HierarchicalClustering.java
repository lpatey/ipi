package fr.imag.ama.ipi.algorithms;

import java.util.ArrayList;
import java.util.Arrays;

import fr.imag.ama.ipi.tools.ClusteringResult;

public class HierarchicalClustering {

	private static final int WARD = 1;

	public static final ClusteringResult compute( float[][] similarityMatrix, double[] members, int centrality ) {

		int n = similarityMatrix.length + 1;
		int len = n*(n-1)/2;
		int[] ia = new int[n];
		int[] ib = new int[n];
		double[] crit = new double[n];
		int[] order = new int[n];
		int[] iia = new int[n];
		int[] iib = new int[n];
		int[] iic = new int[n];
		int[][] merge = new int[n-1][3];
		double[] distanceVector = asDouble( similarityMatrix );
		boolean[] flags = new boolean[n];

		hclust( n, len, WARD, ia, ib, crit, members, new int[n], new double[n], flags, distanceVector );

		hcass2( n, ia, ib, order, iia, iib, iic, similarityMatrix, centrality );

		for( int i=0; i<n-1; i++ ) {
			merge[i][0] = iia[i];
			merge[i][1] = iib[i];
			merge[i][2] = iic[i];
		}

		return new ClusteringResult( merge, order, crit );

	}

	public static final ClusteringResult compute( float[][] similarityMatrix, int centrality ) {
		double[] members = new double[ similarityMatrix.length + 1 ];
		Arrays.fill( members, 1);
		return compute(similarityMatrix, members, centrality );
	}
	
	public static final ClusteringResult compute( float[][] similarityMatrix ) {
		return compute(similarityMatrix, 4 );
	}

	private static final double[] asDouble( float[][] similarityMatrix ) {

		int n = similarityMatrix.length+1;
		int len = n*(n-1)/2;
		double[] diss = new double[len];

		int k=0;
		for( int i=0; i<n-1; i++ ) {
			for( int j=i; j<n-1; j++ ) {
				diss[k++] = 1-similarityMatrix[j][i];
			}
		}

		return diss;
	}

	private static final void hclust( 
			int n, 
			int len, 
			int iopt, 
			int[] ia, 
			int[] ib, 
			double[] crit, 
			double[] membr, 
			int[] nn, 
			double[] disnn, 
			boolean flag[], 
			double[] diss ) {

		int im = 0, jj = 0, jm = 0, i, ncl, j, ind, i2, j2, k, ind1, ind2, ind3;
		double inf = 1e300, dmin, d12;

		// Initializations
		Arrays.fill( flag, true );
		ncl = n;

		/*
		Carry out an agglomeration - first create list of NNs
		Note NN and DISNN are the nearest neighbour and its distance
		TO THE RIGHT of I.
		 */
		for( i=0; i<n-1; i++ ) {
			dmin = inf;
			for( j=i+1; j<n; j++ ) {
				ind = ioffst( n, i, j );
				if( diss[ ind ] >= dmin ) {
					continue;
				}
				dmin = diss[ ind ];
				jm = j;
			}
			nn[i] = jm;
			disnn[i] = dmin;
		}


		do {


			// Next, determine least diss. using list of NNs
			dmin = inf;
			for( i=0; i<n-1; i++ ) {
				if( !flag[i] ) {
					continue;
				}
				if( disnn[i] >= dmin ) {
					continue;
				}
				dmin = disnn[i];
				im = i;
				jm = nn[i];

			}
			ncl--;


			// This allows an agglomeration to be carried out.
			i2 = Math.min( im, jm );
			j2 = Math.max( im, jm );
			ia[n-ncl-1] = i2+1;
			ib[n-ncl-1] = j2+1;
			crit[n-ncl-1] = dmin;
			flag[j2] = false;

			// Update dissimilarities from new cluster.
			dmin = inf;
			for( k=0; k<n; k++ ) {
				if( !flag[k] ) {
					continue;
				}

				if( k == i2 ) {
					continue;
				}

				ind1 = i2 < k ? ioffst( n, i2, k ) : ioffst( n, k, i2 );
				ind2 = j2 < k ? ioffst( n, j2, k ) : ioffst( n, k, j2 );
				ind3 = ioffst( n, i2, j2 );
				d12 = diss[ind3];

				switch( iopt ) {
				case WARD :
					diss[ind1] = (membr[i2]+membr[k])*diss[ind1] + (membr[j2]+membr[k])*diss[ind2] - membr[k]*d12;
					diss[ind1] /= (membr[i2] + membr[j2] + membr[k] );
					break;
				default :
					System.out.println( "Unknown method" );
				}

			}

			membr[i2] += membr[j2];

			// Update list of NNs
			for( i=0; i<n-1; i++ ) {
				if( !flag[i]) {
					continue;
				}
				dmin = inf;
				for( j=i+1; j<n; j++ ) {
					if( !flag[j] ) {
						continue;
					}
					ind = ioffst( n, i, j );
					if( diss[ind] >= dmin ) {
						continue;
					}

					dmin = diss[ind];
					jj = j;

				}
				nn[i] = jj;
				disnn[i] = dmin;
			}

		}
		while( ncl > 1 );

	}

	private static final int ioffst( int n, int i, int j ) {
		return j+i*n-((i+1)*(i+2))/2;
	}

	/**
	 * Adapted from routine HCASS, which additionally determines
	 *	cluster assignments at all levels, at extra comput. expense 
	 */
	private static final void hcass2( int n, int[] ia, int[] ib, int[] iorder, int[] iia, int[] iib, int[] iic, 
			float[][] similarityMatrix, int centrality ) {

		int i, j, k, k1, k2, loc;

		for( i=0; i<n; i++ ) {
			iia[i] = ia[i];
			iib[i] = ib[i];
		}

		for( i=0; i<n-2; i++ ) {
			k = Math.min( ia[i], ib[i] );
			for( j=i+1; j<n-1; j++ ) {
				if( ia[j] == k ) {
					iia[j] = -(i+1);
				}
				if( ib[j] == k ) {
					iib[j] = -(i+1);
				}

			}
		}

		for( i=0; i<n-1; i++ ) {
			iia[i] *= -1;
			iib[i] *= -1;
		}

		for( i=0; i<n-1; i++ ) {
			if( iia[i] > 0 && iib[i] < 0 ) {
				k = iia[i];
				iia[i] = iib[i];
				iib[i] = k;
			}
			if( iia[i] > 0 && iib[i] > 0 ) {
				k1 = Math.min( iia[i],iib[i] );
				k2 = Math.max( iia[i], iib[i] );
				iia[i] = k1;
				iib[i] = k2;
			}

			if( centrality > 0 ) {
				iic[i] = 1 + getTypicalIndex( iia, iib, iic, i, similarityMatrix, centrality );
			}

		}

		//iic[n-1] = getTypicalIndex( iia, iib, iic, n-1, d );

		// NEW PART FOR `ORDER'
		iorder[0] = iia[n-2];
		iorder[1] = iib[n-2];
		loc = 1;

		for( i=n-3; i>=0; i-- ) {
			for( j=0; j<=loc; j++ ) {
				if( iorder[j] == i+1 ) {
					iorder[j] = iia[i];
					if( j==loc ) {
						loc++;
						iorder[loc] = iib[i];
						break;
					}

					loc++;
					for( k=loc; k>=j+2; k-- ) {
						iorder[k] = iorder[k-1];
					}
					iorder[j+1] = iib[i];
					break;

				}

			}
		}

		for( i=0; i<n; i++ ) {
			iorder[i] *= -1;
		}
	}

	private static void getIndexes( ArrayList<Integer> indexes, int[] iia, int[] iib, int[] iic, int index, int level ) {
		
		if( level == 0 ) {
			indexes.add( iic[index]-1 );
			return;
		}
		
		if( iia[index] < 0 ) {
			indexes.add( -1* iia[index]-1 );
		}
		else {
			getIndexes( indexes, iia, iib, iic, iia[index]-1, level-1 );
		}
		
		if( iib[index] < 0 ) {
			indexes.add( -1* iib[index]-1 );
		}
		else {
			getIndexes( indexes, iia, iib, iic, iib[index]-1, level-1 );
		}

	}

	private static int getTypicalIndex( int[] iia, int[] iib, int[] iic, int index, float[][] similarityMatrix, int centrality ) {

		ArrayList<Integer> indexes = new ArrayList<Integer>();
		getIndexes( indexes, iia, iib, iic, index, centrality );
		double maxSquare = -1;
		int size = indexes.size();
		int typicalIndex = 0;
		for( int i=0; i<size; i++ ) {
			double square = 0;
			int indexA = indexes.get(i);
			for( int j=0; j<size; j++ ) {
				int indexB = indexes.get(j);
				if( indexA == indexB ) {
					continue;
				}
				square += Math.pow( similarityMatrix[ (int)Math.max( indexA, indexB ) - 1 ][ (int) Math.min( indexA, indexB ) ], 2 );
			}
			if( square > maxSquare ) {
				maxSquare = square;
				typicalIndex = indexA;
			}
		}

		return typicalIndex;
	}


}
