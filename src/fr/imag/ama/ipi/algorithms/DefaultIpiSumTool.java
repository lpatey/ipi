package fr.imag.ama.ipi.algorithms;

import fr.imag.ama.ipi.algorithms.families.IpiSumTool;

public class DefaultIpiSumTool implements IpiSumTool {

	public final double computeEdgeAndNeighboursSum(double edge, double neighbours) {
		return (edge+neighbours)/2;
	}

	public final double computeVertexAndEdgeSum(double vertex, double edge) {
		return (vertex+edge)/2;
	}

	
}
