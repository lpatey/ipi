package fr.imag.ama.ipi.algorithms.families;

/**
 * Represents a maximal cost solving algorithm
 * Each algorithm of this kind must implement this interface
 * @author The AMA Team
 *
 */
public interface MaximalCostAlgorithm {
	
	/**
	 * Computes the maximal cost algorithm
	 * @param matrix The cost matrix
	 * @return The sum of costs
	 */
	public double compute( double[][] matrix );

}
