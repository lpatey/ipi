package fr.imag.ama.ipi.algorithms.families;

public interface IpiSumTool {
	
	/**
	 * 
	 * @param vertex The similarity indice between two vertices
	 * @param edge The similarity indice between two edges
	 * @return The arithmetic mean
	 */
	public double computeVertexAndEdgeSum( double vertex, double edge );
	
	/**
	 * 
	 * @param edge The similarity indice between two edges
	 * @param neighbours The similarity indice between all neighbours
	 * @return The arithmetic mean
	 */
	public double computeEdgeAndNeighboursSum( double edge, double neighbours );

}
