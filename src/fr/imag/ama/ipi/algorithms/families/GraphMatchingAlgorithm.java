package fr.imag.ama.ipi.algorithms.families;

import fr.imag.ama.ipi.beans.Graph;

public interface GraphMatchingAlgorithm {

	public double compute( Graph a, Graph b, double[][] subsumptionMatrix );
}
