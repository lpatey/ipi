package fr.imag.ama.ipi.algorithms.families;

import fr.imag.ama.ipi.beans.Graph;

public interface SimilarityAlgorithm {

	public double getSimilarityDegree( Graph a, Graph b );
	
	public double getInclusionDegree( Graph a, Graph b );
	
}
