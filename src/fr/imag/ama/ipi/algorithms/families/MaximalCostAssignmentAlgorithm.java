package fr.imag.ama.ipi.algorithms.families;

public interface MaximalCostAssignmentAlgorithm {
	
	public int[][] compute( double[][] matrix );

}
