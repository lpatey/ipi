package fr.imag.ama.ipi.algorithms;

import fr.imag.ama.ipi.algorithms.families.MaximalCostAlgorithm;
import fr.imag.ama.ipi.algorithms.families.MaximalCostAssignmentAlgorithm;
import fr.imag.ama.ipi.tools.HardCodedSum;

/**
 * Adapts any maximal cost assignment algorithm to a maximal cost algorithm
 * by simply computing the sum of assigned items
 * @author The AMA Team
 */
public class AssignmentAdapter implements MaximalCostAlgorithm {
	
	private MaximalCostAssignmentAlgorithm algorithm;
	
	/**
	 * Constructor
	 * @param algorithm The maximal cost assignment algorithm which will be used
	 * to compute the assignment before computing the sum
	 */
	public AssignmentAdapter( MaximalCostAssignmentAlgorithm algorithm ) {
		this.algorithm = algorithm;
	}


	/**
	 * Computes the maximal cost by computing a maximal cost assignment
	 * and then computing the sum of costs
	 */
	public double compute(double[][] matrix) {
		
		if( matrix.length == 0 ) {
			return 0;
		}
		
		if( Math.max( matrix.length, matrix[0].length ) < 5 ) {
			return HardCodedSum.compute( matrix );
		}
		
		int[][] assignments = algorithm.compute(matrix);
		double sum = 0;

		// For each column
		for( int j=0; j<matrix[0].length; j++ ) {
			// Get row
			int i = assignments[j][0];
			if( i<matrix.length) {
				sum += matrix[i][j];
			}
		}

		return sum;
		
	}

}
