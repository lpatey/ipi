package fr.imag.ama.ipi.main;

import fr.imag.ama.ipi.tools.SimilarityMatrixComputer;
import fr.imag.ama.ipi.tools.Verbose;


public class Main {

	private static final float VERSION = 1.0f;
	
	public static void main(String[] args) {
		
		SimilarityMatrixComputer computer = new SimilarityMatrixComputer();
		boolean help = false;
		boolean matrixKindSpecified = false;
			
		/**
		 * Parsing parameters
		 * --input <file>
		 * --output <file> (optional) default --input
		 * --metadata <file> (optional)
		 * --round <integer> (optional) default 7
		 * --threshold <float> (optional) default 10E-3
		 * --matrix <inclusion|similarity>
		 * --clustering (optional)
		 * --centrality <integer> (optional) default 4
		 * --similarity <file1> <file2> [<integer1> <integer2>]
		 * --inclusion <file1> <file2> [<integer1> <integer2>]
		 * --irln (optional)
		 * --seeds <integer>|all (optional) default 10
		 * --verbose (optional)
		 * --help (optional)
		 * --version (optional)
		 */
		for( int i=0; i<args.length; i++ ) {
			if( args[i].toLowerCase().equals( "--input" ) ) {
				computer.setInput( args[++i] );
			}
			else if( args[i].toLowerCase().equals( "--version" ) ) {
				System.out.println( VERSION );
				System.exit(0);
			}
			else if( args[i].toLowerCase().equals( "--output" ) ) {
				computer.setOutput( args[++i] );
			}
			else if( args[i].toLowerCase().equals( "--metadata" )  ) {
				computer.setMetadata( args[++i] );
			}
			else if( args[i].toLowerCase().equals( "--round" ) ) {
				computer.setRound( Integer.parseInt( args[++i] ) );
			}
			else if( args[i].toLowerCase().equals( "--threshold" ) ) {
				computer.setThreshold( Float.parseFloat( args[++i] ) );
			}
			else if( args[i].toLowerCase().equals( "--matrix" ) ) {
				String answer = args[++i];
				matrixKindSpecified = true;
				if( "inclusion".equals( answer.toLowerCase() ) ) {
					computer.setSimilarity( false );
				}
				else if( "similarity".equals( answer.toLowerCase() ) ) {
					computer.setSimilarity( true );
				}
				else {
					throw new Error( "Invalid argument for --matrix : must be inclusion of similarity" );
				}
			}
			else if( args[i].toLowerCase().equals( "--clustering" ) ) {
				if( !matrixKindSpecified ) {
					matrixKindSpecified = true;
					computer.setSimilarity( true );
				}
				computer.setClustering( true );
			}
			else if( args[i].toLowerCase().equals( "--centrality" ) ) {
				if( i+1 < args.length && !args[i+1].startsWith( "--" ) ) {
					computer.setCentrality( Integer.parseInt( args[++i] ) );		
				}
				else {
					computer.setCentrality( 4 );
				}
			}
			else if( args[i].toLowerCase().equals( "--seeds" ) ) {
				if( "all".equals( args[++i].toLowerCase() ) ) {
					computer.setSeeds( -1 );
				}
				else {
					computer.setSeeds( Integer.parseInt( args[i] ) );
				}
			}
			else if( args[i].toLowerCase().equals( "--irln" ) ) {
				computer.setIrln( true );
			}
			else if( args[i].toLowerCase().equals( "--similarity" ) ) {
				computer.setSimilarity( true );
				matrixKindSpecified = true;
				computer.setFile1( args[++i] );
				if( !args[i+1].matches( "^[0-9]+$") ) {
					computer.setFile2( args[++i] );
				}
				else {
					computer.setFile2( computer.getFile1() );
				}

				if( i+1 < args.length && !args[i+1].startsWith( "--" ) ) {
					computer.setIndex1( Integer.parseInt( args[++i] ) );
				}
				if( i+1 < args.length && !args[i+1].startsWith( "--" ) ) {
					computer.setIndex2( Integer.parseInt( args[++i] ) );
				}
			}
			else if( args[i].toLowerCase().equals( "--inclusion" ) ) {
				computer.setSimilarity( false );
				matrixKindSpecified = true;
				computer.setFile1( args[++i] );
				if( !args[i+1].matches( "^[0-9]+$") ) {
					computer.setFile2( args[++i] );
				}
				else {
					computer.setFile2( computer.getFile1() );
				}

				if( i+1 < args.length && !args[i+1].startsWith( "--" ) ) {
					computer.setIndex1( Integer.parseInt( args[++i] ) );
				}
				if( i+1 < args.length && !args[i+1].startsWith( "--" ) ) {
					computer.setIndex2( Integer.parseInt( args[++i] ) );
				}
			}
			else if( args[i].toLowerCase().equals( "--verbose" ) ) {
				if( i+1 < args.length  && !args[i+1].startsWith( "--" ) ) {
					Verbose.setVerboseLevel( Integer.parseInt( args[++i] ) );
				}
				else {
					Verbose.setVerboseLevel( Verbose.MEDIUM_LEVEL );
				}
			}
			else if( args[i].toLowerCase().equals( "--help" ) ) {
				
				help = true;
			}
			else if( args[i].toLowerCase().startsWith( "--" ) ) {
				System.out.println( "Error : unknown option " + args[i] );
				help = true;
			}
			
		}
		
		if( !matrixKindSpecified ) {
			System.out.println( "Error : --matrix option required" );
			help = true;
		}
		

		/**
		 * Help : Gives parameters
		 */
		if( args.length == 0 || help ) {
			System.out.println( "IPI v" + VERSION );
			System.out.println( "Syntax :");
			System.out.println( "java -jar IPI.jar [args...]" );
			System.out.println( "Arguments :");
			System.out.println( " --input <file>" );
			System.out.println( " --output <file> (optional) default --input" );
			System.out.println( " --metadata <file> (optional)" );
			System.out.println( " --round <integer> (optional) default 7" );
			System.out.println( " --threshold <float> (optional) default 10E-3" );
			System.out.println( " --matrix <inclusion|similarity>" );
			System.out.println( " --clustering (optional)" );
			System.out.println( " --centrality <integer> (optional) default 4" );
			System.out.println( " --similarity <file1> <file2> [<integer1> <integer2>]" );
			System.out.println( " --inclusion <file1> <file2> [<integer1> <integer2>]" );
			System.out.println( " --irln (optional)" );
			System.out.println( " --seeds <integer>|all (optional) default 10" );
			System.out.println( " --verbose (optional)" );
			System.out.println( " --help (optional)" );
			System.exit(0);
		}

		
		/**
		 * Computing
		 */
		try {		
			computer.compute();
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
		
	}

}
