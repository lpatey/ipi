package fr.imag.ama.ipi.tools;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Locale;
import java.util.SortedSet;
import java.util.TreeSet;

import fr.imag.ama.ipi.algorithms.AssignmentAdapter;
import fr.imag.ama.ipi.algorithms.GreedyAlgorithm;
import fr.imag.ama.ipi.algorithms.HierarchicalClustering;
import fr.imag.ama.ipi.algorithms.HungarianAlgorithm;
import fr.imag.ama.ipi.algorithms.IpiSimilarity;
import fr.imag.ama.ipi.algorithms.families.GraphMatchingAlgorithm;
import fr.imag.ama.ipi.algorithms.families.MaximalCostAlgorithm;
import fr.imag.ama.ipi.algorithms.families.SimilarityAlgorithm;
import fr.imag.ama.ipi.beans.Environment;
import fr.imag.ama.ipi.beans.Graph;
import fr.imag.ama.ipi.tools.io.GenericReader;
import fr.imag.ama.ipi.tools.io.IRLWriter;
import fr.imag.ama.ipi.tools.io.ReadException;

public class SimilarityMatrixComputer implements ActionListener {
	
	private ArrayList<ActionListener> listeners = new ArrayList<ActionListener>();
	private SimilarityAlgorithm similarityAlgorithm;
	private int[] point = null;
	
	private FileWriter matrixWriter;
	private int graphsNumber;
	private Integer nRunningThreads = 0;
	private Thread[] threads;
	private float[][] similarityMatrix;
	private float[][] inclusionMatrix;
	private int[] progressMatrix;
	private Environment environment;
	private String input = null;
	private String output = null;
	private String metadata = null;
	private float threshold = 10E-3f;
	private boolean similarity = true;
	private boolean clustering = false;
	private boolean irln = false;
	private int centrality = -1;
	private String file1 = null;
	private String file2 = null;
	private int index1 = 0;
	private int index2 = 0;
	private int round = 7;
	private int seeds = 10;
	
	public String getInput() {
		return input;
	}

	public void setInput(String input) {
		this.input = input;
		if( getOutput() == null ) {
			setOutput( input.replaceAll( "\\.[a-z]+$", "" ) );
		}
	}
	
	public void setSeeds( int seeds ) {
		this.seeds = seeds;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}

	public String getMetadata() {
		return metadata;
	}

	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}

	public float getThreshold() {
		return threshold;
	}

	public void setThreshold(float threshold) {
		this.threshold = threshold;
	}

	public boolean isSimilarity() {
		return similarity;
	}

	public void setSimilarity(boolean similarity) {
		this.similarity = similarity;
	}

	public boolean isClustering() {
		return clustering;
	}

	public void setClustering(boolean clustering) {
		this.clustering = clustering;
	}

	public boolean isIrln() {
		return irln;
	}

	public void setIrln(boolean irln) {
		this.irln = irln;
	}

	public int getCentrality() {
		return centrality;
	}

	public void setCentrality(int centrality) {
		this.centrality = centrality;
	}

	public String getFile1() {
		return file1;
	}

	public void setFile1(String file1) {
		this.file1 = file1;
	}

	public String getFile2() {
		return file2;
	}

	public void setFile2(String file2) {
		this.file2 = file2;
	}

	public int getIndex1() {
		return index1;
	}

	public void setIndex1(int index1) {
		this.index1 = index1;
	}

	public int getIndex2() {
		return index2;
	}

	public void setIndex2(int index2) {
		this.index2 = index2;
	}

	public int getRound() {
		return round;
	}

	public void setRound(int round) {
		this.round = round;
	}
	
	public Environment getEnvironment() {
		return environment;
	}
	
	public SimilarityAlgorithm getSimilarityAlgorithm() {
		if( similarityAlgorithm == null ) {
			MaximalCostAlgorithm adapter = new AssignmentAdapter( new HungarianAlgorithm() );
			GraphMatchingAlgorithm greedy = new GreedyAlgorithm( seeds );
			similarityAlgorithm = new IpiSimilarity( adapter, greedy, getThreshold() );
		}
		return similarityAlgorithm;
	}
	
	public float[][] getSimilarityMatrix() {
		return similarityMatrix;
	}
	
	public float[][] getInclusionMatrix() {
		return inclusionMatrix;
	}
	
	public int[] getNextSimilarityPoint() {
		int[] nextPoint = null;
		int[] tmp;

		if( point == null ) {
			return null;
		}
		else if( point[1] == point[0]-1 ) {
			
			if( point[0] < graphsNumber-1 ) {
			
				nextPoint = new int[]{ point[0]+1, 0 };				
				similarityMatrix[ nextPoint[0]-1 ] = new float[ nextPoint[0] ];
				
			}
			
		}
		else {
			nextPoint = new int[] {point[0], point[1]+1 };
		}

		tmp = point;
		point = nextPoint;
		return tmp;
	}
	
	public int[] getNextInclusionPoint() {
		int[] nextPoint = null;
		int[] tmp;

		if( point == null ) {
			return null;
		}
		else if( point[1] == graphsNumber-1 ) {
			
			if( point[0] < graphsNumber-1 ) {
			
				nextPoint = new int[]{ point[0]+1, 0 };				
				inclusionMatrix[ nextPoint[0] ] = new float[ graphsNumber ];
				
				if( point[0] < graphsNumber-2 ) {
					similarityMatrix[ nextPoint[0] ] = new float[ nextPoint[0] +1 ];
				}
			}
			
		}
		else {
			nextPoint = new int[] {point[0], point[1]+1 };
		}

		tmp = point;
		point = nextPoint;
		return tmp;
	}

	public synchronized int[] getNextPoint() {
		if( isSimilarity() ) {
			return getNextSimilarityPoint();
		}
		else {
			return getNextInclusionPoint();
		}
	}
	
	public synchronized void setInclusionDegree( int[] point, float inclusion ) {
		inclusionMatrix[ point[0] ][ point[1] ] = inclusion;
		progressMatrix[ point[0] ]++;
		
		if( progressMatrix[point[0]] == graphsNumber  ) {
			writeMatrixLine( inclusionMatrix[point[0]] );
			inclusionMatrix[ point[0] ] = null;
		}

	}
	
	public synchronized void setSimilarityDegree( int[] point, float similarity ) {
		similarityMatrix[ point[0]-1 ][ point[1] ] = similarity;
		progressMatrix[ point[0]-1 ]++;
		
		if( progressMatrix[point[0]-1] == point[0] ) {
			writeMatrixLine( similarityMatrix[point[0]-1] );
			if( !isClustering() ) {
				similarityMatrix[point[0]-1] = null;
			}
		}
		

	}
	
	private void writeMatrixLine( float[] line ) {
		try {
			Verbose.println( "Writing line...", Verbose.LOW_LEVEL );
			for( float value : line ) {
				matrixWriter.append( String.format( Locale.ENGLISH, "%1." + getRound() + "f ",  value ) );
			}
			matrixWriter.append( "\n" );
			matrixWriter.flush();
			
		}
		catch( IOException e ) {
			e.printStackTrace();
		}
	}
	
	public void compute() throws IOException, ReadException {
		
		GenericReader reader = new GenericReader();
		
		if( getMetadata() != null ) {
			File metadataFile = new File( getMetadata() );
			environment = reader.read( metadataFile );
		}
		
		if( getInput() == null ) {
			computeUnique();
			return;
		}
		
		
		environment = reader.read( new File( getInput() ), environment, null );
		
		if( isIrln() ) {
			IRLWriter writer = new IRLWriter( true );
			writer.write( new File( getOutput() + ".irln" ), environment );
		}
			
			
		/**
		 * Collection size
		 */
		this.graphsNumber = environment.getGraphs().size();
		Verbose.println( "Size : " + graphsNumber , Verbose.MEDIUM_LEVEL );
		
		/**
		 * Matrix initialization
		 */
		similarityMatrix = new float[ graphsNumber - 1 ][];
		progressMatrix = new int[ graphsNumber ];
		similarityMatrix[0] = new float[1];
		File matrixFile = null;
		
		if( isSimilarity() ) {
			point = new int[]{ 1, 0 };
			matrixFile = new File( getOutput() + ".matrix" );
			if( matrixFile.exists( ) ) {
				loadSimilarityMatrix( matrixFile );
			}
			
			File inclusionMatrixFile = new File( getOutput() + ".imatrix" );
			if( inclusionMatrixFile.exists( ) ) {
				loadSimilarityMatrixFromInclusion( inclusionMatrixFile );
			}
		}
		else {
			inclusionMatrix = new float[ graphsNumber ][];
			point = new int[]{ 0, 0 };
			inclusionMatrix[0] = new float[ graphsNumber ];
			matrixFile = new File( getOutput() + ".imatrix" );
			if( matrixFile.exists( ) ) {
				loadInclusionMatrix( matrixFile );
			}
		}

		matrixWriter = new FileWriter( matrixFile, true );
		
		/**
		 * Processors number
		 */
		int nThreads = Runtime.getRuntime().availableProcessors();
		Verbose.println( "There are " + nThreads + " available processors", Verbose.MEDIUM_LEVEL );
		threads = new Thread[ nThreads ];
		
		/**
		 * Launch threads
		 */
		nRunningThreads = nThreads;
		for( int i=0; i<nThreads; i++ ) {
			threads[i] = new SimilarityThread( this );
			threads[i].setPriority( Thread.MAX_PRIORITY );
			threads[i].start();
			
			// Ajout d'un decalage entre 2 lancements de threads
			// pour que la matrice aie eu le temps de se lancer
			// et que l'on évite l'écriture de la seconde ligne
			// avant celle de la premiere ligne.
			try {
				Thread.sleep( 100 );
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		}

		/**
		 * Join threads to be useful
		 */
		for( int i=0; i<nThreads; i++ ) {
			if( threads[i].isAlive() ) {
				try {
					threads[i].join();
				}
				catch( Exception e ) {
					e.printStackTrace();
				}
			}
		}
		
		matrixWriter.close();
	}
	
	private void computeUnique() throws IOException, ReadException {
		
		GenericReader reader = new GenericReader();
		Environment env = null;
		Graph graph1 = null, graph2 = null;
		SortedSet<Integer> set;
		
		if( getFile1().equals( getFile2() ) ) {
			set = new TreeSet<Integer>();
			set.add( getIndex1() );
			set.add( getIndex2() );
			env = reader.read( new File( getFile1() ), environment, set );
			
			if( getIndex1() == getIndex2() ) {
				graph1 = graph2 = env.getGraphs().get(0);
			}
			else {
				boolean ordered = env.getGraphs().get(0).getIndex() == getIndex1();
				graph1 = ordered ? env.getGraphs().get(0) : env.getGraphs().get(1);
				graph2 = ordered ? env.getGraphs().get(1) : env.getGraphs().get(0);
			}
		}
		else {
			set = new TreeSet<Integer>();
			set.add( getIndex1() );
			env = reader.read( new File( getFile1() ), environment, set );
			graph1 = env.getGraphs().get( 0 );
			
			set = new TreeSet<Integer>();
			set.add( getIndex2() );
			env = reader.read( new File( getFile2() ), environment, set );
			graph2 = env.getGraphs().get( 0 );
		}
	
		if( isSimilarity() ) {
			double similarity = getSimilarityAlgorithm().getSimilarityDegree( graph1, graph2 );
			System.out.println(String.format( Locale.ENGLISH, "%1." + getRound() + "f ",  similarity ) );
		}
		else {
			double inclusion = getSimilarityAlgorithm().getInclusionDegree( graph1, graph2 );
			System.out.println(String.format( Locale.ENGLISH, "%1." + getRound() + "f ",  inclusion ) );
		}
		
	}
	
	private void loadSimilarityMatrix( File matrixFile ) throws FileNotFoundException {
		FileReader reader = new FileReader( matrixFile );
		LineNumberReader lnReader = new LineNumberReader( reader );
		String line;
		int i=0;
		int point[] = new int[] {1, 0 };
		
		try {
		
			while( (line = lnReader.readLine() ) != null ) {
				String[] texts = line.trim().split( " " );
				float[] numbers = new float[i+1];
				for( int j=0; j<texts.length; j++ ) {
					numbers[i] = Float.parseFloat( texts[j] );
				}
				similarityMatrix[i] = numbers;
				if( texts.length < i+1 ) {
					System.out.println( "Error : suppress last line from similarity matrix" );
					System.out.println( "Line : " + (i+1) );
					System.exit( 1 );
				}
				
				i++;
			}
			
			lnReader.close();
			reader.close();
		
		}
		catch( IOException e )  {
			e.printStackTrace();
		}
		
		if( i == graphsNumber-1 ) {
			this.point = null;
			return;
		}
		
		point[0] = i+1;
		similarityMatrix[ point[0]-1 ] = new float[ point[0] ];
		Verbose.println( "Starting from (" + point[0] + "," + point[1] + ")", Verbose.MEDIUM_LEVEL );
		this.point = point;
		
	}
	
	private void loadInclusionMatrix( File matrixFile ) throws FileNotFoundException {
		FileReader reader = new FileReader( matrixFile );
		LineNumberReader lnReader = new LineNumberReader( reader );
		String line;
		int i=0;
		int point[] = new int[] {1, 0 };
		
		try {
		
			while( (line = lnReader.readLine() ) != null ) {
				String[] texts = line.trim().split( " " );
				if( texts.length < graphsNumber ) {
					System.out.println( "Error : suppress last line from inclusion matrix" );
					System.exit( 1 );
				}
				i++;
			}
			
			lnReader.close();
			reader.close();
		
		}
		catch( IOException e )  {
			e.printStackTrace();
		}
		
		if( i == graphsNumber )  {
			this.point = null;
			return;
		}
		
		point[0] = i;
		inclusionMatrix[ point[0] ] = new float[ graphsNumber ];
		Verbose.println( "Starting from (" + point[0] + "," + point[1] + ")", Verbose.MEDIUM_LEVEL );
		this.point = point;
	}
	
	private void loadSimilarityMatrixFromInclusion( File inclusionMatrixFile ) throws FileNotFoundException {
		RandomAccessFile reader = new RandomAccessFile(inclusionMatrixFile, "r" );
		File similarityMatrixFile = new File( getOutput() + ".matrix" );
		byte[] number1 = new byte[ getRound()+2 ];
		byte[] number2 = new byte[ getRound()+2 ];
		int[] point;
		this.point = new int[] {1, 0 };
		progressMatrix = new int[graphsNumber];
		
		try {
		
			matrixWriter = new FileWriter( similarityMatrixFile );
			
			while ((point = getNextSimilarityPoint()) != null) {
				
				reader.seek( (point[0] * graphsNumber + point[1] ) * (getRound() + 3) + point[0]);
				reader.read(number1);
				reader.seek( (point[1] * graphsNumber + point[0] ) * (getRound() + 3)+ point[1]);
				reader.read(number2 );
			
				setSimilarityDegree( point, (Float.parseFloat(new String( number1 )) + Float.parseFloat(new String( number2 )))/2 );
			}
			
			reader.close();
			matrixWriter.close();
		}
		catch( IOException e )  {
			e.printStackTrace();
		}
		
	}
	
	public void actionPerformed(ActionEvent e) {

		synchronized ( nRunningThreads ) {
			nRunningThreads--;
		}
		if( nRunningThreads > 0 ) {
			return;
		}

		e.setSource( this );
		
		try {
			
			float[][] similarityMatrix = getSimilarityMatrix();
			
			if( isClustering() ) {
			
				if( !isSimilarity() ) {
					File matrixFile = new File( getOutput() + ".imatrix" );
					if( matrixFile.exists( ) ) {
						loadSimilarityMatrixFromInclusion( matrixFile );
					}
				}
				
				Verbose.println( "Computing clusters...", Verbose.MEDIUM_LEVEL );
				ClusteringResult result = HierarchicalClustering.compute( similarityMatrix, getCentrality() );
			
				int[][] merge = result.getMerge();
				double[] dist = result.getHeight();
				FileWriter writer2 = new FileWriter( new File( getOutput() + ".clusters" ) );
				int length = ("" + graphsNumber).length()+1;
				String format = "%1$" + length + "d %2$" + length + "d %3$1." + getRound() + "f" + (getCentrality() > 0 ? " %4$" + length + "d" : "" ) + "\n";
				double maxHeight = 0;
				for( int i=0; i<merge.length; i++) {
					maxHeight = Math.max( maxHeight, dist[i] );
				}
				int i = 0;
				for( int[] row : merge ) {
					writer2.write( String.format( Locale.ENGLISH, format, row[0],row[1], dist[i]/maxHeight, row[2] ) );
					i++;
				}
				writer2.close();
			
			}
			
		
		}
		catch( IOException exception ) {
			exception.printStackTrace();
		}
		
		for( ActionListener l : listeners ) {
			l.actionPerformed( e );
		}

	}
	
	
	public void addActionListener( ActionListener listener ) {
		listeners.add( listener );
	}

}
