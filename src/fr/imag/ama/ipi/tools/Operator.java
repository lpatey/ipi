package fr.imag.ama.ipi.tools;

public interface Operator {
	
	double compute( double[] values );
}
