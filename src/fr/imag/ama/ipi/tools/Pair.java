package fr.imag.ama.ipi.tools;

/**
 * Represents a couple of objects
 * @author The AMA Team
 *
 * @param <E> The first object's data type
 * @param <F> The second object's data type
 */
public final class Pair<E,F> {
	
	private E first;
	private F second;
	
	/**
	 * Instanciates the couple with the two objects
	 * @param first The first object
	 * @param second The second object
	 */
	public Pair( E first, F second ) {
		this.first = first;
		this.second = second;
	}
	
	/**
	 * Returns the first object
	 * @return The first object
	 */
	public final E getFirst() {
		return first;
	}
	
	/**
	 * Returns the second object
	 * @return The second object
	 */
	public final F getSecond() {
		return second;
	}
	

}
