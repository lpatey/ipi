package fr.imag.ama.ipi.tools;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import fr.imag.ama.ipi.algorithms.families.SimilarityAlgorithm;
import fr.imag.ama.ipi.beans.Graph;

public class SimilarityThread extends Thread {

	private SimilarityMatrixComputer computer;
	private ArrayList<ActionListener> listeners = new ArrayList<ActionListener>();
	
	public SimilarityThread( SimilarityMatrixComputer computer ) {
		this.computer = computer;
		addActionListener( computer );
	}
	
	public void addActionListener( ActionListener listener ) {
		listeners.add( listener );
	}
	
	@Override
	public void run() {
		SimilarityMatrixComputer computer = this.computer;
		SimilarityAlgorithm algorithm = computer.getSimilarityAlgorithm();
		int[] point;
		ArrayList<Graph> graphs = computer.getEnvironment().getGraphs();
		while ((point = computer.getNextPoint()) != null) {
			Graph first = graphs.get( point[0] );
			Graph second = graphs.get( point[1] );
			if( computer.isSimilarity() ) {
				computer.setSimilarityDegree( point, (float) algorithm.getSimilarityDegree( first, second ) );
			}
			else {
				computer.setInclusionDegree( point,  (float) (first == second ? 1 : algorithm.getInclusionDegree( first, second ) ) );
			}
		}

		Verbose.println( "Thread stopped", Verbose.MEDIUM_LEVEL );
		
		ActionEvent e = new ActionEvent( this, 0, "" );
		for( ActionListener l : listeners ) {
			l.actionPerformed( e );
		}

	}
}
