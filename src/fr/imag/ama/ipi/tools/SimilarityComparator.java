package fr.imag.ama.ipi.tools;

import java.util.Comparator;


public class SimilarityComparator implements Comparator<int[]> {
	
	private double[][] matrix;
	
	public SimilarityComparator( double[][] matrix ) {
		this.matrix = matrix;
	}

	public int compare(int[] o1, int[] o2) {
		
		return (int) (1000000* (matrix[o1[0]][o1[1]] - matrix[o2[0]][o2[1]] ) );
	}

}
