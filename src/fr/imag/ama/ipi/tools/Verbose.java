package fr.imag.ama.ipi.tools;

public class Verbose {
	
	public static final int NO_VERBOSE = 0;
	public static final int LOW_LEVEL = 1;
	public static final int MEDIUM_LEVEL = 2;
	public static final int HIGH_LEVEL = 3;
	
	private static int level = 0;
	
	public static void setVerboseLevel( int level ) {
		Verbose.level = level;
	}

	public static void println( String message, int level ) {
		if( Verbose.level >= level ) {
			System.out.println( message );
		}
	}
}
