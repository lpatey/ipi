package fr.imag.ama.ipi.tools;

import fr.imag.ama.ipi.beans.Vertex;

public class GreedyAlgorithmSeed implements Comparable<GreedyAlgorithmSeed>{
	
	private Vertex vertexA;
	private Vertex vertexB;
	private double localSimilarity;

	public GreedyAlgorithmSeed( Vertex vertexA, Vertex vertexB, double localSimilarity ) {
		this.vertexA = vertexA;
		this.vertexB = vertexB;
		this.localSimilarity = localSimilarity;
	}

	public final Vertex getVertexA() {
		return vertexA;
	}

	public final Vertex getVertexB() {
		return vertexB;
	}

	public final double getLocalSimilarity() {
		return localSimilarity;
	}

	/**
	 * Inversed comparaison to obtain a decreasing sort
	 */
	public final int compareTo(GreedyAlgorithmSeed seed) {
		return getLocalSimilarity() < seed.getLocalSimilarity() ? 1 : -1;

	}

}
