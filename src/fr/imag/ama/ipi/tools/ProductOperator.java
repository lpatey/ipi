package fr.imag.ama.ipi.tools;

public class ProductOperator implements Operator {

	
	public double compute(double[] values) {
		double result = 1;
		for( int i=0; i<values.length; i++ ) {
			result *= values[i];
		}
		return result;
	}

}
