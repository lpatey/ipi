package fr.imag.ama.ipi.tools;

public class AverageOperator implements Operator {

	public double compute(double[] values) {
		double result = 0;
		for( int i=0; i<values.length; i++ ) {
			result += values[i];
		}
		return result/values.length;
	}

}
