package fr.imag.ama.ipi.tools;

public class ClusteringResult {
	
	private int[][] merge;
	private int[] order;
	private double[] height;
	
	public ClusteringResult( int[][] merge, int[] order, double[] height ) {
		this.merge = merge;
		this.order = order;
		this.height = height;
	}
	
	public double[] getHeight() {
		return height;
	}
	
	public int[][] getMerge() {
		return merge;
	}
	
	public int[] getOrder() {
		return order;
	}

}
