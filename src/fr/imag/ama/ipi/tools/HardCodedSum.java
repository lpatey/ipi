package fr.imag.ama.ipi.tools;

/**
 * Tool class which permit to compute little assignment problem by hard code.
 * @author The AMA Team
 *
 */
public final class HardCodedSum {

	/**
	 * Solves an assignment problem to return the maximum sum
	 * @param matrix A matrix whose dimensions are smaller than 5
	 * @return The maximum sum
	 */
	public final static double compute( double[][] matrix ) {

		double sum = 0;
		double tmp;

		if (matrix[0].length == 1)
		{
			for (int i = 0 ; i < matrix.length ; i++) {
				tmp = matrix[i][0];
				if( sum < tmp )
					sum = tmp;
			}
			return sum;
		}
		else if (matrix.length == 1)
		{
			for (int i = 0 ; i < matrix[0].length ; i++) {
				tmp = matrix[0][i];
				if( sum < tmp )
					sum = tmp;
			}
			return sum;
		}
		else if (matrix.length == 2 && matrix[0].length == 2)
		{
			tmp = matrix[0][0]+matrix[1][1];
			if( sum < tmp )
				sum = tmp;
			tmp = matrix[1][0]+matrix[0][1];
			if( sum < tmp )
				sum = tmp;

			return sum;
		}
		else if (matrix.length == 3 && matrix[0].length == 2)
		{
			tmp = matrix[0][0]+matrix[1][1];
			if( sum < tmp )
				sum = tmp;
			tmp = matrix[0][0]+matrix[2][1];
			if( sum < tmp )
				sum = tmp;
			tmp = matrix[1][0]+matrix[0][1];
			if( sum < tmp )
				sum = tmp;
			tmp = matrix[1][0]+matrix[2][1];
			if( sum < tmp )
				sum = tmp;
			tmp = matrix[2][0]+matrix[0][1];
			if( sum < tmp )
				sum = tmp;
			tmp = matrix[2][0]+matrix[1][1];
			if( sum < tmp )
				sum = tmp;
			return sum;
		}
		else if (matrix.length == 4 && matrix[0].length == 2)
		{
			tmp = matrix[0][0] + matrix[1][1];
			if( sum < tmp )
				sum = tmp;
			tmp = matrix[0][0] + matrix[2][1];
			if( sum < tmp )
				sum = tmp;
			tmp = matrix[0][0] + matrix[3][1];
			if( sum < tmp )
				sum = tmp;
			tmp = matrix[1][0] + matrix[0][1];
			if( sum < tmp )
				sum = tmp;
			tmp = matrix[1][0] + matrix[2][1];
			if( sum < tmp )
				sum = tmp;
			tmp = matrix[1][0] + matrix[3][1];
			if( sum < tmp )
				sum = tmp;
			tmp = matrix[2][0] + matrix[0][1];
			if( sum < tmp )
				sum = tmp;
			tmp = matrix[2][0] + matrix[1][1];
			if( sum < tmp )
				sum = tmp;
			tmp = matrix[2][0] + matrix[3][1];
			if( sum < tmp )
				sum = tmp;
			tmp = matrix[3][0] + matrix[0][1];
			if( sum < tmp )
				sum = tmp;
			tmp = matrix[3][0] + matrix[1][1];
			if( sum < tmp )
				sum = tmp;
			tmp = matrix[3][0] + matrix[2][1];
			if( sum < tmp )
				sum = tmp;

			return sum;
		}
		else if (matrix.length == 4 && matrix[0].length == 3)
		{
			tmp =  matrix[0][0]+ matrix[1][1]+ matrix[2][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[0][0]+ matrix[1][1]+ matrix[3][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[0][0]+ matrix[2][1]+ matrix[1][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[0][0]+ matrix[2][1]+ matrix[3][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[0][0]+ matrix[3][1]+ matrix[1][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[0][0]+ matrix[3][1]+ matrix[2][2];
			if( sum < tmp )
				sum = tmp;

			tmp =  matrix[1][0]+ matrix[0][1]+ matrix[2][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[1][0]+ matrix[0][1]+ matrix[3][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[1][0]+ matrix[2][1]+ matrix[0][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[1][0]+ matrix[2][1]+ matrix[3][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[1][0]+ matrix[3][1]+ matrix[0][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[1][0]+ matrix[3][1]+ matrix[2][2];
			if( sum < tmp )
				sum = tmp;

			tmp =  matrix[2][0]+ matrix[0][1]+ matrix[1][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[2][0]+ matrix[0][1]+ matrix[3][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[2][0]+ matrix[1][1]+ matrix[0][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[2][0]+ matrix[1][1]+ matrix[3][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[2][0]+ matrix[3][1]+ matrix[0][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[2][0]+ matrix[3][1]+ matrix[1][2];
			if( sum < tmp )
				sum = tmp;

			tmp =  matrix[3][0]+ matrix[0][1]+ matrix[1][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[3][0]+ matrix[0][1]+ matrix[2][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[3][0]+ matrix[1][1]+ matrix[0][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[3][0]+ matrix[1][1]+ matrix[2][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[3][0]+ matrix[2][1]+ matrix[0][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[3][0]+ matrix[2][1]+ matrix[1][2];
			if( sum < tmp )
				sum = tmp;

			return sum;
		}
		else if (matrix.length == 3 && matrix[0].length == 4)
		{
			tmp =  matrix[0][0]+ matrix[1][1]+ matrix[2][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[0][0]+ matrix[1][1]+ matrix[2][3];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[0][0]+ matrix[2][1]+ matrix[1][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[0][0]+ matrix[2][1]+ matrix[1][3];
			if( sum < tmp )
				sum = tmp;

			tmp =  matrix[1][0]+ matrix[0][1]+ matrix[2][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[1][0]+ matrix[0][1]+ matrix[2][3];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[1][0]+ matrix[2][1]+ matrix[0][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[1][0]+ matrix[2][1]+ matrix[0][3];
			if( sum < tmp )
				sum = tmp;

			tmp =  matrix[2][0]+ matrix[1][1]+ matrix[0][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[2][0]+ matrix[1][1]+ matrix[0][3];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[2][0]+ matrix[0][1]+ matrix[1][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[2][0]+ matrix[0][1]+ matrix[1][3];
			if( sum < tmp )
				sum = tmp;

			return sum;
		}
		else if (matrix.length == 4 && matrix[0].length == 4)
		{
			tmp =  matrix[0][0]+ matrix[1][1]+  matrix[2][2]+ matrix[3][3];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[0][0]+ matrix[1][1]+ matrix[3][2]+ matrix[2][3];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[0][0]+ matrix[2][1]+ matrix[1][2]+ matrix[3][3];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[0][0]+ matrix[2][1]+ matrix[3][2]+ matrix[1][3];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[0][0]+ matrix[3][1]+ matrix[1][2]+ matrix[2][3];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[0][0]+ matrix[3][1]+ matrix[2][2]+ matrix[1][3];
			if( sum < tmp )
				sum = tmp;

			tmp =  matrix[1][0]+ matrix[0][1]+ matrix[2][2]+ matrix[3][3];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[1][0]+ matrix[0][1]+ matrix[3][2]+ matrix[2][3];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[1][0]+ matrix[2][1]+ matrix[0][2]+ matrix[3][3];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[1][0]+ matrix[2][1]+ matrix[3][2]+ matrix[0][3];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[1][0]+ matrix[3][1]+ matrix[0][2]+ matrix[2][3];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[1][0]+ matrix[3][1]+ matrix[2][2]+ matrix[0][3];
			if( sum < tmp )
				sum = tmp;

			tmp =  matrix[2][0]+ matrix[0][1]+ matrix[1][2]+ matrix[3][3];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[2][0]+ matrix[0][1]+ matrix[3][2]+ matrix[1][3];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[2][0]+ matrix[1][1]+ matrix[0][2]+ matrix[3][3];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[2][0]+ matrix[1][1]+ matrix[3][2]+ matrix[0][3];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[2][0]+ matrix[3][1]+ matrix[0][2]+ matrix[1][3];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[2][0]+ matrix[3][1]+ matrix[1][2]+ matrix[0][3];
			if( sum < tmp )
				sum = tmp;

			tmp =  matrix[3][0]+ matrix[0][1]+ matrix[1][2]+ matrix[2][3];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[3][0]+ matrix[0][1]+ matrix[2][2]+ matrix[1][3];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[3][0]+ matrix[1][1]+ matrix[0][2]+ matrix[2][3];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[3][0]+ matrix[1][1]+ matrix[2][2]+ matrix[0][3];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[3][0]+ matrix[2][1]+ matrix[0][2]+ matrix[1][3];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[3][0]+ matrix[2][1]+ matrix[1][2]+ matrix[0][3];
			if( sum < tmp )
				sum = tmp;


			return sum;
		}
		else if (matrix.length == 2 && matrix[0].length == 3)
		{
			tmp =  matrix[0][0]+ matrix[1][1];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[0][0]+ matrix[1][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[1][0]+ matrix[0][1];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[1][0]+ matrix[0][2];
			if( sum < tmp )
				sum = tmp;
			return sum;

		}
		else if (matrix.length == 2 && matrix[0].length == 4)
		{
			tmp =  matrix[0][0]+ matrix[1][1];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[0][0]+ matrix[1][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[0][0]+ matrix[1][3];
			if( sum < tmp )
				sum = tmp;

			tmp =  matrix[1][0]+ matrix[0][1];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[1][0]+ matrix[0][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[1][0]+ matrix[0][3];
			if( sum < tmp )
				sum = tmp;

			return sum;

		}
		else if (matrix.length == 3 && matrix[0].length == 3)
		{
			tmp =  matrix[0][0]+ matrix[1][1]+ matrix[2][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[0][0]+ matrix[2][1]+ matrix[1][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[1][0]+ matrix[0][1]+ matrix[2][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[1][0]+ matrix[2][1]+ matrix[0][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[2][0]+ matrix[0][1]+ matrix[1][2];
			if( sum < tmp )
				sum = tmp;
			tmp =  matrix[2][0]+ matrix[1][1]+ matrix[0][2];
			if( sum < tmp )
				sum = tmp;

			return sum;
		}

		return 0;

	}

}
