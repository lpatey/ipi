package fr.imag.ama.ipi.tools;

import java.lang.reflect.Constructor;


public class OperatorFactory {
	
	private OperatorFactory() {
		
	}

	public static OperatorFactory getFactory() {
		return new OperatorFactory();
	}
	
	public Operator newOperator( String name ) {
		try {
			
			Class<?> typeClass = Class.forName( this.getClass().getPackage().getName() + "." + name + "Operator" );
			Constructor<?> c = typeClass.getConstructors()[0];
			return (Operator) c.newInstance( new Object[] {} );
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
		
		return null;
	}
}
