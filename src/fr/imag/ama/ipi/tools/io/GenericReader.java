package fr.imag.ama.ipi.tools.io;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.SortedSet;

import fr.imag.ama.ipi.beans.Environment;
import fr.imag.ama.ipi.tools.io.chemistry.SDFReader;

public class GenericReader implements Reader {
	
	public Environment read(File file, Environment env, SortedSet<Integer> indexes) throws ReadException, IOException {
		
		FileReader reader = new FileReader( file );
		LineNumberReader lnReader = new LineNumberReader( reader );
	
		boolean isIRL = lnReader.readLine().matches( "<\\?[^>]*\\?>" );
		
		lnReader.close();
		reader.close();
		
		
		if( isIRL ) {
			return new IRLReader().read(file, env, indexes );
		}
		else {
			return new SDFReader().read(file, env, indexes );
			
		}
	}
	
	public Environment read(File file) throws ReadException, IOException {
		return read( file, null, null );
	}
	
}
