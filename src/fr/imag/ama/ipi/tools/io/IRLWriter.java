package fr.imag.ama.ipi.tools.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map.Entry;
import java.util.Properties;

import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

import org.apache.xml.serializer.Method;
import org.apache.xml.serializer.OutputPropertiesFactory;
import org.apache.xml.serializer.Serializer;
import org.apache.xml.serializer.SerializerFactory;

import fr.imag.ama.ipi.beans.Edge;
import fr.imag.ama.ipi.beans.EdgeType;
import fr.imag.ama.ipi.beans.Environment;
import fr.imag.ama.ipi.beans.Graph;
import fr.imag.ama.ipi.beans.GraphType;
import fr.imag.ama.ipi.beans.Vertex;
import fr.imag.ama.ipi.beans.VertexType;
import fr.imag.ama.ipi.types.ComposedType;
import fr.imag.ama.ipi.types.SimpleType;
import fr.imag.ama.ipi.types.Type;

public class IRLWriter implements Writer {

	private final static String VERSION = "0.1";
	private boolean fullTags = false;
	
	public IRLWriter( boolean fullTags ) {
		this.fullTags = fullTags;
	}
	
	public IRLWriter() {
		this( false );
	}

	public void write( File file, Environment env) throws IOException {
		FileOutputStream fos = new FileOutputStream( file );
		Properties props = 
				OutputPropertiesFactory.getDefaultMethodProperties(Method.XML);
		Serializer serializer = SerializerFactory.getSerializer(props);
		ContentHandler hd = serializer.asContentHandler();
		
		try {
			hd.startDocument();
			
			AttributesImpl collectionAttributes = new AttributesImpl();
			collectionAttributes.addAttribute( "", "", "version", "CDATA", VERSION );
			hd.startElement( "", "", "collection", collectionAttributes );
			
			/* Headers */
			if( !env.getHeaders().isEmpty() ) {
				hd.startElement( "", "", "headers", null );
				for( Entry<String, String> entry : env.getHeaders().entrySet() ) {
					AttributesImpl headerAttributes = new AttributesImpl();
					headerAttributes.addAttribute( "", "", "value", "CDATA", entry.getValue() );
					hd.startElement( "", "", entry.getKey(), headerAttributes );
					hd.endElement( "", "", entry.getKey() );
				}
				hd.endElement( "", "", "headers" );
			}
			
			
			/* Declarations */
			hd.startElement( "", "",  "declare", null );
			
				/* Macros */
				if( !env.getMacros().isEmpty() || this.fullTags ) {
					hd.startElement( "", "", "macros", null );
					for( Type t : env.getMacros() ) {
						if( t instanceof SimpleType ) {
							SimpleType st = (SimpleType) t;
							AttributesImpl atts = new AttributesImpl();
							atts.addAttribute( "", "", "method", "CDATA", st.getClass().getSimpleName().replaceAll( "Type$", "" ) );
							if( ! "".equals(st.getDomain()) || this.fullTags ) {
								atts.addAttribute( "", "", "domain", "CDATA", st.getDomain() );
							}
							hd.startElement( "", "", st.getName(), atts );
							writeInclusions( hd,  st );
							hd.endElement( "", "", st.getName() );
						}
						else {
							ComposedType ct = (ComposedType) t;
							hd.startElement( "", "", ct.getName(), null );
							writeProperties( hd, ct, env );
							writeInclusions( hd, ct );
							hd.endElement( "", "", ct.getName() );
							
						}
					}
					hd.endElement( "", "", "macros" );
				}
				
				
				/* Graphs */
				if( !env.getVerticesTypes().isEmpty() || this.fullTags ) {
					hd.startElement( "", "", "graphs", null );
					for( GraphType gt : env.getGraphTypes() ) {
						hd.startElement( "", "", gt.getName(), null );
						writeProperties( hd, gt, env );
						writeInclusions( hd, gt );
						hd.endElement( "", "", gt.getName() );
					}
					hd.endElement( "", "", "graphs" );
				}
				
				
				/* Classes */
				if( !env.getVerticesTypes().isEmpty() || this.fullTags ) {
					hd.startElement( "", "", "classes", null );
					for( VertexType vt : env.getVerticesTypes() ) {
						hd.startElement( "", "", vt.getName(), null );
						writeProperties( hd, vt, env );
						writeInclusions( hd,  vt );
						hd.endElement( "", "", vt.getName() );
					}
					hd.endElement( "", "", "classes" );
				}
				
				/* Relations */
				if( !env.getEdgesTypes().isEmpty() || this.fullTags ) {
					hd.startElement( "", "", "relations", null );
					for( EdgeType et : env.getEdgesTypes() ) {
						hd.startElement( "", "", et.getName(), null );
						if( et.getVerticesTypes().length > 0 || this.fullTags ) {
							AttributesImpl verticesTypesAtts = new AttributesImpl();
							if( et.isDirected() ) {
								verticesTypesAtts.addAttribute( "", "", "directed", "CDATA", "true" );
							}
							hd.startElement( "", "", "vertices-types", verticesTypesAtts );
							int length = et.getVerticesTypes().length;
							for( int i=0; i<length; i++ ) {
								AttributesImpl subAtts = new AttributesImpl();
								if( et.getVerticesLabels() != null ) {
									subAtts.addAttribute( "", "", "role", "CDATA", et.getVerticesLabels()[i] );
								}
								hd.startElement( "", "", et.getVerticesTypes()[i].getName(), subAtts);
								hd.endElement( "", "", et.getVerticesTypes()[i].getName() );
							}
							hd.endElement( "", "", "vertices-types" );
						}
						writeProperties( hd, et, env );
						writeInclusions( hd,  et );
						hd.endElement( "", "", et.getName() );
					}
					hd.endElement( "", "", "relations" );
				}
			
			hd.endElement( "", "", "declare" );
			
			/* Graphs */
			hd.startElement( "", "", "graphs",  null );
			for( Graph g : env.getGraphs() ) {
				AttributesImpl atts = new AttributesImpl();
				atts.addAttribute( "", "", "id", "CDATA", g.getName() );
				hd.startElement( "", "", g.getType().getName(), atts );
				if( g.getVertices().length > 0 || this.fullTags ) {
					hd.startElement( "", "", "vertices", null );
					for( Vertex v : g.getVertices() ) {
						AttributesImpl vertexAtts = new AttributesImpl();
						vertexAtts.addAttribute( "", "", "id", "CDATA", v.getName() );
						hd.startElement( "", "", v.getType().getName(), vertexAtts );
						String values = v.getValueAsString();
						hd.characters( values.toCharArray(), 0, values.length() );
						hd.endElement( "", "", v.getType().getName() );
					}
					hd.endElement( "", "", "vertices" );
				}
				
				if( g.getEdges().length > 0 || this.fullTags ) {
					hd.startElement( "", "", "edges", null );
					for( Edge e : g.getEdges() ) {
						AttributesImpl edgeAtts = new AttributesImpl();
						if( ! "".equals(e.getName()) ) {
							edgeAtts.addAttribute( "", "", "id", "CDATA", e.getName() );
						}
						hd.startElement( "", "", e.getType().getName(), edgeAtts );
						String values = e.getValueAsString();
						String vertices = e.getVerticesAsString();
						hd.startElement( "", "", "vertices", null );
						hd.characters( vertices.toCharArray(), 0, vertices.length() );
						hd.endElement( "", "", "vertices" );
						hd.startElement( "", "", "properties", null );
						hd.characters( values.toCharArray(), 0, values.length() );
						hd.endElement( "", "", "properties" );
						hd.endElement( "", "", e.getType().getName() );
					}
					hd.endElement( "", "", "edges" );
				}
				
				if( g.getType().getTypes().length > 0  || this.fullTags ) {
					hd.startElement( "", "", "properties", null );
					String values = g.getValueAsString();
					hd.characters( values.toCharArray(), 0, values.length() );
					hd.endElement( "", "", "properties" );
				}
				
				hd.endElement( "", "", g.getType().getName() );
			}
			hd.endElement( "", "", "graphs" );
			
			
			hd.endElement( "", "", "collection" );
			hd.endDocument();
		
		}
		catch( SAXException e ) {
			e.printStackTrace();
		}
		
		fos.close();
		

	}
	
	private void writeProperties( ContentHandler hd, ComposedType t, Environment env ) throws SAXException {
		if( t.getTypes().length > 0 || this.fullTags ) {
			AttributesImpl atts = new AttributesImpl();
			String operator = t.getOperator().getClass().getSimpleName().replaceAll( "Operator$", "" );
			if( !env.getDefaultSimilarityOperator().equals( operator ) ) {
				atts.addAttribute( "", "", "similarity-operator", "CDATA", operator );
			}
			hd.startElement( "", "", "properties", atts );
			int length = t.getTypes().length;
			for( int i=0; i<length; i++ ) {
				AttributesImpl subAtts = new AttributesImpl();
				subAtts.addAttribute( "", "", "type", "CDATA", t.getTypes()[i].getName() );
				hd.startElement( "", "", t.getLabels()[i] , subAtts);
				hd.endElement( "", "", t.getLabels()[i] );
			}
			hd.endElement( "", "", "properties" );
		}
	}
	
	private void writeInclusions( ContentHandler hd, Type t ) throws SAXException {
		if( !t.getInclusions().isEmpty() || this.fullTags ) {
			hd.startElement( "", "", "inclusions", null );
			for( Entry<String, Double> inc : t.getInclusions().entrySet() ) {
				AttributesImpl incAtts = new AttributesImpl();
				incAtts.addAttribute( "", "", "value", "CDATA", inc.getValue().toString() );
				hd.startElement( "", "", inc.getKey(), incAtts );
				hd.endElement( "", "", inc.getKey() );
			}
			hd.endElement( "", "", "inclusions" );
		}
	}

}
