package fr.imag.ama.ipi.tools.io;

import java.io.File;
import java.io.IOException;

import fr.imag.ama.ipi.beans.Environment;

/**
 * Represents an abstract Reader which will return a list of I
 * @author The AMA Team
 *
 * @param <I>
 */
public interface Reader {
	
	public Environment read( File file ) throws ReadException, IOException;

}
