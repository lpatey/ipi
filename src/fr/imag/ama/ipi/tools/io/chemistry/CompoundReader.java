package fr.imag.ama.ipi.tools.io.chemistry;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;

import fr.imag.ama.ipi.beans.Edge;
import fr.imag.ama.ipi.beans.EdgeType;
import fr.imag.ama.ipi.beans.Environment;
import fr.imag.ama.ipi.beans.Graph;
import fr.imag.ama.ipi.beans.GraphType;
import fr.imag.ama.ipi.beans.Vertex;
import fr.imag.ama.ipi.beans.VertexType;
import fr.imag.ama.ipi.tools.AverageOperator;
import fr.imag.ama.ipi.tools.OperatorFactory;
import fr.imag.ama.ipi.tools.io.ReadException;
import fr.imag.ama.ipi.tools.io.Reader;
import fr.imag.ama.ipi.types.EnumType;
import fr.imag.ama.ipi.types.IntegerType;
import fr.imag.ama.ipi.types.InvalidDomainException;
import fr.imag.ama.ipi.types.InvalidInstanceException;
import fr.imag.ama.ipi.types.Type;

/**
 * Parser for the following format :
 * 
 * <pre>
 * compound(1-pyrimethamine).
 * atm(1-pyrimethamine,1-pyrimethamine_1,N,2,0,2,2,false,true).
 * atm(1-pyrimethamine,1-pyrimethamine_2,C,3,0,2,3,false,true).
 * ...
 * bond(1-pyrimethamine,1-pyrimethamine_1,1-pyrimethamine_2,4,true,true,false,false,false,false,false).
 * bond(1-pyrimethamine,1-pyrimethamine_1,1-pyrimethamine_6,4,true,true,false,false,false,false,false).
 * ...
 * :- active(1-pyrimethamine).
 * </pre>
 * 
 * @author The AMA Team
 *
 */
@Deprecated
public class CompoundReader implements Reader {
	
	public Environment read( File file ) throws ReadException, IOException {
		
		Environment env = new Environment();
		GraphType graphType = null;
		VertexType atomType = null;
		EdgeType bondType = null;
		
		try {
			
			graphType = new GraphType( "molecule", null, new Type[] {}, new AverageOperator(), env );
			
			EnumType symbolType = new EnumType( "chemicalSymbol", 
				"{H,He,Li,Be,B,C,N,O,F,Ne,Na,Mg,Al,Si,P,S,Cl,Ar,K,"
				+"Ca,Sc,Ti,V,Cr,Mn,Fe,Co,Ni,Cu,Zn,Ga,Ge,As,Se,Br,Kr,"
				+"Rb,Sr,T,Zr,Nb,Mo,Tc,Ru,Rh,Pd,Ag,Cd,In,Sn,Sb,Te,I,Xe,"
				+"Cs,Ba,Hf,Ta,W,Re,Os,Ir,Pt,Au,Hg,TI,Pb,Bi,Po,At,Rn,Fr,"
				+"Ra,Rf,Db,Sg,Bh,Hs,Mt,Ds,Rg,Uub,Uut,Uuq,Uup,Uuh,Uus,Uuo,"
				+" La,Ce,Pr,Nd,Pm,Sm,Eu,Gd,Tb,Dy,Ho,Er,Tm,Yb,Lu,Ac,Th,Pa,U,"
				+"Np,Pu,Am,Cm,Bk,Cf,Es,Fm,Md,No,Lr", env );
			atomType = new VertexType( "atom", new String[] { "symbol" }, new Type[] { symbolType }, new AverageOperator(), env );
			
			IntegerType valence = new IntegerType( "valence", "[0,4]", env);
			bondType = new EdgeType( "bond", 
					new String[] { "valence" }, new Type[] { valence }, 
					null, new VertexType[] { atomType, atomType }, 
					OperatorFactory.getFactory().newOperator( "Average" ), false, env );
		}
		catch( InvalidDomainException e ) {
			e.printStackTrace();
		}
		
		FileReader reader = new FileReader(file);
		LineNumberReader lnReader = new LineNumberReader(reader);
		String line;
		int index = 0;
		
		while( (line = lnReader.readLine()) != null ) {
			
			line = line.trim();
			if( line.startsWith( "compound" ) ) {
				try {
					String name = line.substring(line.indexOf("(")+1, line.indexOf(")"));
					Graph m = parseGraph( name, lnReader, index, graphType, atomType, bondType );
					m.computeOptimizations();
					env.addGraph( m );
					index++;
				}
				catch( InvalidInstanceException e ) {
					e.printStackTrace();
				}
			}
			
		}
		lnReader.close();
		reader.close();
		

		return env;
	}
	
	private Graph parseGraph( String name, LineNumberReader reader, int index, GraphType graphType, VertexType atomType, EdgeType bondType ) throws IOException, InvalidInstanceException {
		
		Graph molecule = graphType.getValue( name, index, "{}" );
		String line;
		int vertexIndex = 0;
		int edgeIndex = 0;
		
		while( (line = reader.readLine()) != null ) {
			
			line = line.trim();
			if( line.startsWith( "atm" ) ) {
				line = line.substring(line.indexOf("(")+1, line.indexOf(")"));
				molecule.add( parseVertex( line, molecule, vertexIndex++, atomType ) );
			}
			else if( line.startsWith("bond") ) {
				line = line.substring(line.indexOf("(")+1, line.indexOf(")"));
				molecule.add( parseEdge( line, molecule, edgeIndex++, bondType ) );
			}
			else {
				return molecule;
			}
			
		}

		return null;
		
	}
	
	private Vertex parseVertex( String line, Graph molecule, int index, VertexType atomType ) throws InvalidInstanceException {
		
		String[] infos = line.split( "," );
		Vertex atom = atomType.getValue( "atom-" + index, molecule, index, "{" + infos[2] + "}" );
		return atom;
	}
	
	private Edge parseEdge( String line, Graph molecule, int index, EdgeType bondType ) throws InvalidInstanceException {
		
		String[] infos = line.split( "," );
		Vertex[] atoms = new Vertex[2];
		
		// Link atoms to covalent bonds
		for( int i=0; i<2; i++ ) {
			int idx = Integer.parseInt( infos[i+1].substring(infos[i+1].indexOf("_")+1) ) -1;
			atoms[i] = molecule.getVertices()[ idx ];
		}
		
		Edge bond = bondType.getValue( molecule, index, atoms, "{" + infos[3] + "}" );
		for( Vertex atom : atoms ) {
			atom.addEdge( bond );
		}
		return bond;
	}
	
}
