package fr.imag.ama.ipi.tools.io.chemistry;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.SortedSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.imag.ama.ipi.beans.Edge;
import fr.imag.ama.ipi.beans.EdgeType;
import fr.imag.ama.ipi.beans.Environment;
import fr.imag.ama.ipi.beans.Graph;
import fr.imag.ama.ipi.beans.GraphType;
import fr.imag.ama.ipi.beans.Vertex;
import fr.imag.ama.ipi.beans.VertexType;
import fr.imag.ama.ipi.tools.AverageOperator;
import fr.imag.ama.ipi.tools.OperatorFactory;
import fr.imag.ama.ipi.tools.Verbose;
import fr.imag.ama.ipi.tools.io.ReadException;
import fr.imag.ama.ipi.tools.io.Reader;
import fr.imag.ama.ipi.types.EnumType;
import fr.imag.ama.ipi.types.FloatType;
import fr.imag.ama.ipi.types.IntegerType;
import fr.imag.ama.ipi.types.InvalidDomainException;
import fr.imag.ama.ipi.types.InvalidInstanceException;
import fr.imag.ama.ipi.types.StringType;
import fr.imag.ama.ipi.types.Type;

/**
 * Parser for the following format :
 * 
 * <pre>
 * lit-1
 * Marvin  03190709322D          
 *
 * 21 24  0  0  1  0            999 V2000
 *  -2.5271   -0.3244    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
 *  -2.5271   -1.1494    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
 *  -1.8126   -1.5619    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
 *  -1.8126    0.0881    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
 *  -1.0982   -0.3244    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
 *   1  2  4  0  0  0  0
 *	 1  4  4  0  0  0  0
 *
 * $$$$
 * </pre>
 * 
 * @author The AMA Team
 *
 */
public class SDFReader implements Reader {
	
	private float ENUM_PERCENT_THRESHOLD = 10;
	
	private Pattern prePattern = Pattern.compile( "^(.{3})(.{3})", Pattern.DOTALL );
	private Pattern keyPattern = Pattern.compile( "^>  <(.*)>$", Pattern.DOTALL );
	
	private Pattern vertexPattern = Pattern.compile( "^(.{10})(.{10})(.{10})(.{3})(.{3})(.{3})(.{3})(.{3})(.{3})(.{3})(.{3})(.{3})(.{3})(.{3})(.{3})(.{3})$", Pattern.DOTALL );
	private Pattern edgePattern = Pattern.compile( "^(.{3})(.{3})(.{3})(.{3})?(.{3})?(.{3})?(.{3})?$", Pattern.DOTALL );

	private HashMap<String, String> params = new HashMap<String, String>();
	
	public Environment read( File file ) throws ReadException, IOException {
		return read( file, null, null );
	}
	
	public Environment read( File file, Environment parentEnv, SortedSet<Integer> indexes ) throws ReadException, IOException {
		
		Environment env = new Environment();
		GraphType graphType = null;
		VertexType atomType = null;
		EdgeType bondType = null;
		FileReader reader = null;
		LineNumberReader lnReader = null;
		String line;

		env.inherit( parentEnv );
		
		if( parentEnv == null) {

			Verbose.println( "Detecting SDF meta", Verbose.HIGH_LEVEL );
			try {
				
				EnumType symbolType = new EnumType( "chemicalSymbol", 
					"{H,He,Li,Be,B,C,N,O,F,Ne,Na,Mg,Al,Si,P,S,Cl,Ar,K,"
					+"Ca,Sc,Ti,V,Cr,Mn,Fe,Co,Ni,Cu,Zn,Ga,Ge,As,Se,Br,Kr,"
					+"Rb,Sr,T,Zr,Nb,Mo,Tc,Ru,Rh,Pd,Ag,Cd,In,Sn,Sb,Te,I,Xe,"
					+"Cs,Ba,Hf,Ta,W,Re,Os,Ir,Pt,Au,Hg,TI,Pb,Bi,Po,At,Rn,Fr,"
					+"Ra,Rf,Db,Sg,Bh,Hs,Mt,Ds,Rg,Uub,Uut,Uuq,Uup,Uuh,Uus,Uuo,"
					+"La,Ce,Pr,Nd,Pm,Sm,Eu,Gd,Tb,Dy,Ho,Er,Tm,Yb,Lu,Ac,Th,Pa,U,"
					+"Np,Pu,Am,Cm,Bk,Cf,Es,Fm,Md,No,Lr,L,A,Q,*,LP,R#", env );
				atomType = new VertexType( "atom", new String[] { "symbol" }, new Type[] { symbolType }, new AverageOperator(), env );
				
				IntegerType bondTypeType = new IntegerType( "bondType", "[0,8]", env);
				bondType = new EdgeType( "bond", 
							new String[] { "bondType" }, new Type[] { bondTypeType }, 
							null, new VertexType[] { atomType, atomType }, 
							OperatorFactory.getFactory().newOperator( "Average" ), false, env );
			
			
				
				/** Find properties **/
				HashMap<String,Type> types = new HashMap<String,Type>();
				HashMap<String,HashSet<String>> domains = new HashMap<String,HashSet<String>>();
				reader = new FileReader(file);
				lnReader = new LineNumberReader(reader);
				Matcher m;
				int nItems = 0;
				while( (line = lnReader.readLine()) != null ) {
					m = keyPattern.matcher( line );
					
					if( line.startsWith( "$$$" )) {
						nItems++;
						continue;
					}
					
					if( m.matches() ) {
						String key = m.group(1);
						String value = lnReader.readLine();
						if( !types.containsKey(key) ) {
							types.put( key, null );
						}
						
						Type t = types.get( key );
						if( (t == null || t instanceof IntegerType ) && value.matches( "^[0-9]+$") ) {
							if( t == null ) {
								String domain = "[" + value + "," + value + "]";
								types.put( key, new IntegerType( "prop-" + key, domain, env ) );
							}
							else {
								IntegerType type = (IntegerType) types.get( key );
								int v = Integer.parseInt( value );
								if( type.getMin() > v ) {
									type.setDomain( "[" + value + "," + type.getMax() + "]" );
								}
								else if( type.getMax() < v ) {
									type.setDomain( "[" + type.getMin() + "," + value + "]" );
								}
							}
						}
						else if( (t == null || t instanceof IntegerType || t instanceof FloatType ) && value.matches( "^[0-9\\.]+$" ) ) {
							String domain = "[" + value + "," + value + "]";
							if( t == null ) {
								types.put( key, new FloatType( "prop-" + key, domain, env ) );
							}
							else if( t instanceof IntegerType ) {
								IntegerType integerType = (IntegerType) t;
								float v = Float.parseFloat( value );
								if( integerType.getMin() > v ) {
									domain = "[" + value + "," + integerType.getMax() + "]";
								}
								else if( integerType.getMax() < v ) {
									domain = "[" + integerType.getMin() + "," + value + "]";
								}
								types.put( key, new FloatType( "prop-" + key, domain, env ) );
							}
							else {
								FloatType type = (FloatType) types.get( key );
								float v = Float.parseFloat( value );
								if( type.getMin() > v ) {
									type.setDomain( "[" + value + "," + type.getMax() + "]" );
								}
								else if( type.getMax() < v ) {
									type.setDomain( "[" + type.getMin() + "," + value + "]" );
								}
							}
							
						}
						else if( t == null ) {
							types.put( key, new StringType( "prop-" + key, env ) );
							domains.put( key, new HashSet<String>() );
						}
						
						// Storing distinct values to detect enum
						if( types.get( key ) instanceof StringType ) {
							domains.get( key ).add( value );
						}

					}
					
				}
				lnReader.close();
				reader.close();
				
				for( Map.Entry<String, HashSet<String>> entry : domains.entrySet() ) {
					if( entry.getValue().size() <= nItems*ENUM_PERCENT_THRESHOLD/100 ) {
						String domain = "";
						for( String value : entry.getValue() ) {
							domain += ",\"" + value + "\""; 
						}
						domain = "{" + domain.substring( 1 ) + "}";
						types.put( entry.getKey(), new EnumType(  "prop-" + entry.getKey(), domain, env ) );
					}
				}
				
				graphType = new GraphType( "molecule", types.keySet().toArray( new String[ types.size() ] ), 
						types.values().toArray( new Type[ types.size() ] ), new AverageOperator(), env );
				
			}
			catch( InvalidDomainException e ) {
				e.printStackTrace();
			}
			
		}
		
		reader = new FileReader(file);
		lnReader = new LineNumberReader(reader);
		
		int index = 0;
		
		while( (line = lnReader.readLine()) != null ) {
			
			try {
				
				if( indexes != null && !indexes.contains( index ) ) {
					
					while( (line = lnReader.readLine()) != null ) {
						if( line.startsWith( "$$$" )) {
							break;
						}
					}
					index++;
					continue;
				}
				
				Graph m = parseGraph( lnReader, line.trim(), index, graphType, atomType, bondType );
				//IpiSimilarityTest.N_GRAPHS++;
				m.computeOptimizations();
				env.addGraph( m );
				index++;
			}
			catch( InvalidInstanceException e ) {
				e.printStackTrace();
			}
			catch( OutOfMemoryError e ) {
				Runtime.getRuntime().gc();
				long max = Runtime.getRuntime().maxMemory();
				System.out.println( "Increase max heap size by adding the -Xmxvalue argument" );
				System.out.println( "Example : -Xmx1024m" );
				System.out.println( "You need approximately " + max + " bytes to store " + env.getGraphs().size() + " molecules");
				e.printStackTrace();
				System.exit(0);
			}
		}
		lnReader.close();
		reader.close();

		return env;
	}
	
	private Graph parseGraph( LineNumberReader reader, String name, int index, GraphType graphType, VertexType atomType, EdgeType bondType ) throws IOException, InvalidInstanceException {
		
		
		String line;
		int vertexIndex = 0;
		int edgeIndex = 0;
		String[] vertices;
		String[] edges;
		Matcher m;
		params.clear();
		reader.readLine();
		reader.readLine();
		line = reader.readLine();
		m = prePattern.matcher( line );
		m.find();
		
		int nVertices = Integer.parseInt( m.group( 1 ).trim() );
		int nEdges = Integer.parseInt( m.group( 2 ).trim() );
		
		vertices = new String[ nVertices ];
		edges = new String[ nEdges ];
		
		for( int i=0; i<nVertices; i++ ) {
			vertices[i] = reader.readLine();
		}
		for( int i=0; i<nEdges; i++ ) {
			edges[i] = reader.readLine();
		}
		
		while( (line = reader.readLine()) != null ) {

			if( line.startsWith( "$$$" )) {
				break;
			}
			m = keyPattern.matcher( line );
			if( m.matches() ) {
				String value = reader.readLine();
				if( "".equals( name ) && "compound_identifier".equals( m.group(1) ) ) {
					name = value;
				}
				params.put( "prop-" + m.group(1), value);
			}
		}

		Graph molecule = graphType.getValue( name, index,  params );
		
		for( int i=0; i<nVertices; i++ ) {
			molecule.add( parseVertex( vertices[i], molecule, vertexIndex++, atomType ) );
		}
		for( int i=0; i<nEdges; i++ ) {
			molecule.add( parseEdge( edges[i], molecule, edgeIndex++, bondType ) );
		}
		

		return molecule;
		
	}
	
	private Vertex parseVertex( String line, Graph molecule, int index, VertexType atomType ) throws InvalidInstanceException {
		
		Matcher m = vertexPattern.matcher( line );
		m.find();
		Vertex atom = atomType.getValue( "atom-" + index, molecule, index, "{" + m.group(4).trim() + "}" );
		return atom;
	}
	
	private Edge parseEdge( String line, Graph molecule, int index, EdgeType bondType ) throws InvalidInstanceException {
	
		Matcher m = edgePattern.matcher( line );
		Vertex[] atoms = new Vertex[2];
		
		m.find();
		
		// Link atoms to covalent bonds
		for( int i=0; i<2; i++ ) {
			atoms[i] = molecule.getVertices()[ Integer.parseInt( m.group(i+1).trim() ) - 1 ];
			
		}
		
		Edge bond = bondType.getValue( molecule, index, atoms, "{" + Integer.parseInt(m.group(3).trim()) + "}" );
		for( Vertex atom : atoms ) {
			atom.addEdge( bond );
		}
		return bond;

	}
	
}
