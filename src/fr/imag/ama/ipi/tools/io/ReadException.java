package fr.imag.ama.ipi.tools.io;

/**
 * Exception thrown when there is a syntax error in the parsing file
 * @author Acer
 *
 */
public class ReadException extends Exception {

	private static final long serialVersionUID = 1L;

	public ReadException( String message ) {
		super( message );
	}
	
}
