package fr.imag.ama.ipi.tools.io;

import java.io.File;
import java.io.IOException;

import fr.imag.ama.ipi.beans.Environment;

/**
 * Represents an abstract Writer which will write a list to a file
 * @author The AMA Team
 *
 * @param <I>
 */
public interface Writer {
	
	public void write( File file, Environment e ) throws IOException;

}
