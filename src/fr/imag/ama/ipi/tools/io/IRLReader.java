package fr.imag.ama.ipi.tools.io;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.SortedSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.imag.ama.ipi.beans.Edge;
import fr.imag.ama.ipi.beans.EdgeType;
import fr.imag.ama.ipi.beans.Environment;
import fr.imag.ama.ipi.beans.Graph;
import fr.imag.ama.ipi.beans.GraphType;
import fr.imag.ama.ipi.beans.Vertex;
import fr.imag.ama.ipi.beans.VertexType;
import fr.imag.ama.ipi.tools.ArrayParser;
import fr.imag.ama.ipi.tools.Operator;
import fr.imag.ama.ipi.tools.OperatorFactory;
import fr.imag.ama.ipi.tools.Unique;
import fr.imag.ama.ipi.types.ComposedType;
import fr.imag.ama.ipi.types.SimpleType;
import fr.imag.ama.ipi.types.Type;
import fr.imag.ama.ipi.types.TypeFactory;


public class IRLReader implements Reader {

	public Environment read( File file ) throws ReadException, IOException {
		return read( file, null, null );
	}
	
	public Environment read( File file, Environment env, SortedSet<Integer> indexes ) throws ReadException, IOException {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			
			Document document = builder.parse( file );
			
			
			env = getEnvironment( document, env, indexes );
			
			document = null;
			System.gc();
			
			return env;
		}
		catch( Exception e ) {
			e.printStackTrace();
			throw new ReadException( e.getMessage() );
		}
	}
	
	private Environment getEnvironment( Document doc, Environment parentEnv, SortedSet<Integer> indexes ) {
		
		Environment env = new Environment();
		env.inherit( parentEnv );
		
		Element collection = doc.getDocumentElement();
		NodeList nodes = collection.getChildNodes();
		
		for( int i=0; i<nodes.getLength(); i++ ) {
			Node node = nodes.item(i);
			if( ! (node instanceof Element) ) {
				continue;
			}
			
			Element element = (Element) node;
			if( is("headers", element ) ) {
				addHeaders( element, env );
				continue;
			}
			
			if( is( "declare", element ) ) {
				addDeclare( element, env );
			}
			
			try {
			
				if( is( "graphs", element ) ) {
					addGraphs( element, env, indexes );
				}
			
			}
			catch( OutOfMemoryError e ) {
				Runtime.getRuntime().gc();
				long max = Runtime.getRuntime().maxMemory();
				System.out.println( "Increase max heap size by adding the -Xmxvalue argument" );
				System.out.println( "Example : -Xmx1024m" );
				System.out.println( "You need approximately " + max + " bytes to store " + env.getGraphs().size() + " graphs");
				e.printStackTrace();
				System.exit(0);
			}
			
		}
		
		return env;
	}

	private void addHeaders( Element headers, Environment env ) {
		
		NodeList nodes = headers.getChildNodes();
		
		for( int i=0; i<nodes.getLength(); i++ ) {
			Node node = nodes.item(i);
			if( ! (node instanceof Element) ) {
				continue;
			}
			Element element = (Element) node;
			env.addHeader( element.getTagName(), getElementValue( element ) );
		}
	}
	
	private void addDeclare( Element declare, Environment env ) {
		
		NodeList nodes = declare.getChildNodes();
		
		for( int i=0; i<nodes.getLength(); i++ ) {
			Node node = nodes.item(i);
			if( ! (node instanceof Element) ) {
				continue;
			}
			Element element = (Element) node;
			if( is( "macros", node ) ) {
				addMacros( element, env );
				continue;
			}
			
			if( is( "graphs", node ) ) {
				addGraphsTypes( element, env );
				continue;
			}
			
			if( is( "classes", node ) ) {
				addVerticesTypes( element, env );
				continue;
			}
			
			if( is( "relations", node ) ) {
				addEdgesTypes( element, env );
				continue;
			}
		}
	}
	
	private void addGraphs( Element graphs, Environment env, SortedSet<Integer> indexes ) {
		
		NodeList nodes = graphs.getChildNodes();
		for( int i=0; i<nodes.getLength(); i++ ) {
			Node node = nodes.item(i);
			if( ! (node instanceof Element) ) {
				continue;
			}

			if( indexes != null && !indexes.contains( i ) ) {
				continue;
			}
			
			addGraph( (Element) node, i, env );
		}
			
			
	}
	
	private void addGraph( Element graph, int index, Environment env ) {
		GraphType graphType = env.getGraphType( graph.getTagName() );
		
		try {
			String properties = getPropertiesValue(graph, graphType);
			Graph g = graphType.getValue( graph.getAttribute( "id" ), index, properties);

			Element vertices = getElementByTagName( graph, "vertices" );
			if( vertices != null )  {
				addVertices( vertices, env, g );
			}
			
			Element edges = getElementByTagName( graph, "edges" );
			if( edges != null ) {
				addEdges( edges, env, g );
			}
			
			g.computeOptimizations();
			env.addGraph( g );

		}
		catch( Exception e ) {
			e.printStackTrace();
		}
	}
	
	private void addVertices( Element vertices, Environment env, Graph graph ) {
		
		NodeList nodes = vertices.getChildNodes();
		
		for( int i=0; i<nodes.getLength(); i++ ) {
			Node node = nodes.item(i);
			if( ! (node instanceof Element) ) {
				continue;
			}
			addVertex( (Element) node, env, graph  );
		}
	}
	
	private void addEdges( Element edges, Environment env, Graph graph ) {

		NodeList nodes = edges.getChildNodes();
		
		for( int i=0; i<nodes.getLength(); i++ ) {
			Node node = nodes.item(i);
			if( ! (node instanceof Element) ) {
				continue;
			}
			addEdge( (Element) node, env, graph );
		}
	}
	
	private void addVertex( Element vertex, Environment env, Graph graph ) {
		VertexType vertexType = env.getVertexType( vertex.getTagName() );
		
		try {
			int index = graph.getVertices().length;
			String properties = getPropertiesValue( vertex, vertexType );

			Vertex v = vertexType.getValue( vertex.getAttribute( "id" ), graph, index, properties);
			graph.add( v );
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
	}
	
	private void addEdge( Element edge, Environment env, Graph graph ) {
		EdgeType edgeType = env.getEdgeType( edge.getTagName() );
		
		try {
			String properties = getPropertiesValue( edge, edgeType );
			int index = graph.getEdges().length;
			Vertex[] verticesList = new Vertex[edgeType.getVerticesTypes().length];
			Element vertices = getElementByTagName( edge, "vertices" );
			if( vertices != null ) {
				String[] values = ArrayParser.parse( vertices.getTextContent() );
				for( int i=0; i<verticesList.length; i++ ) {
					verticesList[i] = graph.getVertexByName( values[i] );
				}
			}
			Edge e = edgeType.getValue( edge.getAttribute( "id" ), graph, index, verticesList, properties);
			graph.add( e );
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
	}
	
	private String getPropertiesValue( Element e, ComposedType type ) {
		
		Element properties = getPropertiesElement( e );
		
		if( properties == null ) {
			if( e.getElementsByTagName( "*" ).getLength() == 0 ) {
				return e.getTextContent();
			}
			else {
				properties = e;
			}
		}
		
		if( properties.getElementsByTagName( "*" ).getLength() == 0 ) {
			return properties.getTextContent();
		}
		String ret = "";
		for( int i=0; i<type.getTypes().length; i++ ) {
			Type t = type.getTypes()[i];
			String label = type.getLabels()[i];
			
			Element child = getElementByTagName( e, label );
			if( child == null ) {
				ret += ",?";
			}
			else {
				if( t instanceof SimpleType ) {
					ret += "," + getElementValue(child);
				}
				else {
					ret += "," + getPropertiesValue(child, (ComposedType) t );
				}
			}
		}
		return "{" + ("".equals(ret) ? ret : ret.substring( 1 )) + "}";
	}
	
	
	private void addMacros( Element macros, Environment env ) {
		
		NodeList nodes = macros.getChildNodes();
		
		for( int i=0; i<nodes.getLength(); i++ ) {
			Node node = nodes.item(i);
			if( ! (node instanceof Element) ) {
				continue;
			}
			Element element = (Element) node;
			Type macro;
			if( element.hasAttribute( "method" ) ) {
				macro = getMacro( element, env );
			}
			else {
				macro = getComposedMacro( element, env );
			}
			env.addMacro( macro );
			
		}

	}
	
	private HashMap<String, Type> getProperties( Element macros, Environment env ) {
		
		HashMap<String, Type> map = new HashMap<String, Type>();
		
		if( macros == null ) {
			return map;
		}
		
		NodeList nodes = macros.getChildNodes();
		
		for( int i=0; i<nodes.getLength(); i++ ) {
			Node node = nodes.item(i);
			if( ! (node instanceof Element) ) {
				continue;
			}
			Element element = (Element) node;
			Type macro;
			if( element.hasAttribute( "type" ) ) {
				macro = env.getMacro( element.getAttribute( "type" ) );
			}
			else if( element.hasAttribute( "method" ) ) {
				macro = getInlineMacro( element, env );
				env.addMacro( macro );
			}
			else {
				macro = getInlineComposedMacro( element, env );
				env.addMacro( macro );
			}
			map.put( element.getTagName(), macro );
			
		}
		
		return map;

	}
	
	
	private void addVerticesTypes( Element classes, Environment env ) {
		
		NodeList nodes = classes.getChildNodes();
		
		for( int i=0; i<nodes.getLength(); i++ ) {
			Node node = nodes.item(i);
			if( ! (node instanceof Element) ) {
				continue;
			}
			Element element = (Element) node;
			env.addVertexType( getVertexType( element, env ) );
		}
	}
	
	private void addGraphsTypes( Element classes, Environment env ) {
		
		NodeList nodes = classes.getChildNodes();
		
		for( int i=0; i<nodes.getLength(); i++ ) {
			Node node = nodes.item(i);
			if( ! (node instanceof Element) ) {
				continue;
			}
			Element element = (Element) node;
			env.addGraphType( getGraphType( element, env ) );
		}
	}
	
	private void addEdgesTypes( Element classes, Environment env ) {
		
		NodeList nodes = classes.getChildNodes();
		
		for( int i=0; i<nodes.getLength(); i++ ) {
			Node node = nodes.item(i);
			if( ! (node instanceof Element) ) {
				continue;
			}
			Element element = (Element) node;
			env.addEdgeType( getEdgeType( element, env ) );
		}
	}
	
	private SimpleType getMacro( Element element, Environment env ) {
		TypeFactory factory = TypeFactory.getFactory();
		SimpleType type = factory.newSimpleType( element.getAttribute( "method" ), element.getTagName(), element.getAttribute( "domain" ), env );
		addInclusions( type, element, env );
		return type;
	}
	
	private SimpleType getInlineMacro( Element element, Environment env ) {
		TypeFactory factory = TypeFactory.getFactory();
		SimpleType type = factory.newSimpleType( element.getAttribute( "method" ), "untitled" + Unique.getValue(), element.getAttribute( "domain" ), env );
		addInclusions( type, element, env );
		return type;
	}
	
	private ComposedType getComposedMacro( Element element, Environment env ) {
		Element propertiesElement = getPropertiesElement( element );
		HashMap<String, Type> props = getProperties( propertiesElement, env );
		String opName = getSimilarityOperator( propertiesElement, env);
		Operator op = OperatorFactory.getFactory().newOperator( opName );
		ComposedType type = new ComposedType( element.getTagName(), props.keySet().toArray( new String[props.size()]), 
				props.values().toArray( new Type[props.size()]), op, env );
		addInclusions( type, element, env );
		return type;
	}
	
	private ComposedType getInlineComposedMacro( Element element, Environment env ) {
		Element propertiesElement = getPropertiesElement( element );
		HashMap<String, Type> props = getProperties( propertiesElement, env );
		String opName = getSimilarityOperator( propertiesElement, env);
		Operator op = OperatorFactory.getFactory().newOperator( opName );
		ComposedType type = new ComposedType( "untitled" + Unique.getValue(), 
					props.keySet().toArray( new String[props.size()]), 
					props.values().toArray( new Type[props.size()]), op, env );
		addInclusions( type, element, env );
		return type;
	}
	
	private VertexType getVertexType( Element element, Environment env ) {
		Element propertiesElement = getPropertiesElement( element );
		HashMap<String, Type> props = getProperties( propertiesElement, env );
		String opName = getSimilarityOperator( propertiesElement, env);
		Operator op = OperatorFactory.getFactory().newOperator( opName );
		VertexType type = new VertexType( element.getTagName(), props.keySet().toArray( new String[ props.size() ]), 
				props.values().toArray( new Type[ props.size() ]), op, env );
		addInclusions( type, element, env );
		return type;
	}
	
	private GraphType getGraphType( Element element, Environment env ) {
		Element propertiesElement = getPropertiesElement( element );
		HashMap<String, Type> props = getProperties( propertiesElement, env );
		String opName = getSimilarityOperator( propertiesElement, env);
		Operator op = OperatorFactory.getFactory().newOperator( opName );
		GraphType type = new GraphType( element.getTagName(), props.keySet().toArray( new String[props.size()] ), 
				props.values().toArray( new Type[props.size()]), op, env );
		addInclusions( type, element, env );
		return type;
	}
	
	private EdgeType getEdgeType( Element element, Environment env ) {
		Element propertiesElement = getPropertiesElement( element );
		Element verticesTypesElement = getVerticesTypesElement( element );
		HashMap<String, Type> props = getProperties( propertiesElement, env );
		ArrayList<String> verticesLabels = new ArrayList<String>();
		ArrayList<VertexType> verticesTypes = new ArrayList<VertexType>();
		getEdgeVerticesTypes( verticesLabels, verticesTypes, verticesTypesElement, env );
		
		String opName = getSimilarityOperator( propertiesElement, env);
		Operator op = OperatorFactory.getFactory().newOperator( opName );
		
		EdgeType type;
		boolean isDirected = "true".equals( verticesTypesElement.getAttribute( "directed") );
		
		type = new EdgeType( element.getTagName(), props.keySet().toArray( new String[props.size()]), 
					props.values().toArray( new Type[props.size()]), 
					verticesLabels.isEmpty() ? null : verticesLabels.toArray( new String[verticesLabels.size()]),
					verticesTypes.toArray( new VertexType[ verticesTypes.size()]), op, isDirected, env );
		
		
		addInclusions( type, element, env );
		return type;
	}

	
	
	
	private String getElementValue( Element element ) {
		return element.hasAttribute( "value" ) ? element.getAttribute( "value" ) : element.getTextContent();
	}
	
	private Element getPropertiesElement( Element element ) {
		return getElementByTagName( element, "properties" );
	}
	
	private Element getElementByTagName( Element element, String tagName ) {
		
		NodeList nodes = element.getChildNodes();
		
		for( int i=0; i<nodes.getLength(); i++ ) {
			Node node = nodes.item(i);
			if( ! (node instanceof Element) ) {
				continue;
			}
			Element e = (Element) node;
			if( tagName.toLowerCase().equals( e.getTagName().toLowerCase() ) ) {
				return e;
			}
		}
		return null;
	}
	
	private void getEdgeVerticesTypes( ArrayList<String> labels, ArrayList<VertexType> vertices,
			Element element, Environment env ) {
		
		NodeList nodes = element.getChildNodes();
		
		for( int i=0; i<nodes.getLength(); i++ ) {
			Node node = nodes.item(i);
			if( ! (node instanceof Element) ) {
				continue;
			}
			Element e = (Element) node;
			if( e.hasAttribute( "role" ) ) {
				labels.add( e.getAttribute( "role" ) );
			}
			vertices.add( env.getVertexType( e.getTagName() ) );
		}
		
	}
	
	private Element getInclusionsElement( Element element ) {

		NodeList nodes = element.getChildNodes();
		
		for( int i=0; i<nodes.getLength(); i++ ) {
			Node node = nodes.item(i);
			if( ! (node instanceof Element) ) {
				continue;
			}
			Element e = (Element) node;
			if( "inclusions".equals( e.getTagName().toLowerCase() ) ) {
				return e;
			}
		}
		return null;
	}
	
	private void addInclusions( Type type, Element element, Environment env ) {

		Element inclusions = getInclusionsElement( element );
		
		if( inclusions == null ) {
			return;
		}
		
		NodeList nodes = inclusions.getChildNodes();
		
		for( int i=0; i<nodes.getLength(); i++ ) {
			Node node = nodes.item(i);
			if( ! (node instanceof Element) ) {
				continue;
			}
			Element e = (Element) node;
			type.addInclusion( e.getTagName(), Double.valueOf(getElementValue( element )) ) ;
		}
	}
	
	private Element getVerticesTypesElement( Element element ) {
		
		NodeList nodes = element.getChildNodes();
		
		for( int i=0; i<nodes.getLength(); i++ ) {
			Node node = nodes.item(i);
			if( ! (node instanceof Element) ) {
				continue;
			}
			Element e = (Element) node;
			if( "vertices-types".equals( e.getTagName().toLowerCase() ) ) {
				return e;
			}
		}
		return null;
	}
	
	private String getSimilarityOperator( Element e, Environment env ) {
		return e!= null && e.hasAttribute( "similarity-operator" ) ? e.getAttribute( "similarity-operator") : env.getDefaultSimilarityOperator();
	}
	
	private boolean is( String text, Node node ) {
		return node instanceof Element && text.equals( ((Element) node).getTagName().toLowerCase() );
	}
}
