package fr.imag.ama.ipi.tools;

import java.util.HashMap;
import java.util.Map.Entry;

import fr.imag.ama.ipi.beans.Vertex;

/**
 * Represents a matching between an hypothesis and an example
 * @author The AMA Team
 *
 * @param <V> The vertex class
 * @param <E> The edge class
 * @param <G> The graph class
 */
public final class Matching {
	
	
	private HashMap<Vertex,Vertex> sigma;
	private Vertex[] vertices;
	
	/**
	 * Instanciates a substitution of a range of variables
	 * @param vertices The list of variable
	 */
	public Matching( Vertex[] vertices ) {
		this.vertices = vertices;
		HashMap<Vertex, Vertex> sigma = new HashMap<Vertex, Vertex>( vertices.length );
		for( Vertex vertex : vertices ) {
			sigma.put( vertex,	null );
		}
		this.sigma = sigma;
	}
	
	/**
	 * Computes the subsumption index and returns it
	 * @return The subsumption index ( Between 0 and 1 )
	 */
	public final double getSubsumptionIndex() {
		double sum = 0;
		HashMap<Vertex, Vertex> sigma = this.sigma;
		for( Entry<Vertex, Vertex> e : sigma.entrySet() ) {
			if( e.getValue() != null ) {
				sum++;
			}
		}
		
		return sum/sigma.size();
	}
	
	public void clear() {
		HashMap<Vertex,Vertex> sigma = this.sigma;
		sigma.clear();
		Vertex[] vertices = this.vertices;
		for( Vertex vertex : vertices ) {
			sigma.put( vertex,	null );
		}
	}
	
	/**
	 * Assigns a value to a variable
	 * @param vertexA The variable
	 * @param vertexB The value
	 */
	public final void setSigma( Vertex vertexA, Vertex vertexB ) {
		sigma.put( vertexA, vertexB );
	}
	
	/**
	 * Returns the value associated to a variable
	 * @param vertexA The variable
	 * @return The value
	 */
	public final Vertex getSigma( Vertex vertexA ) {
		return sigma.get( vertexA );
	}
	
	/**
	 * Checks whether a variable has a value
	 * @param vertexA The variable
	 * @return
	 */
	public final boolean hasMatching( Vertex vertexA ) {
		return getSigma( vertexA ) != null;
	}
	
	/**
	 * Checks whether the value is associated to a variable
	 * @param vertexB The value
	 * @return
	 */
	public final boolean hasMatcher( Vertex vertexB ) {
		return sigma.containsValue(vertexB);
	}
	
	/**
	 * Returns the whole map
	 * @return The map between variables and values
	 */
	public final HashMap<Vertex,Vertex> getSigma() {
		return sigma;
	}

}
