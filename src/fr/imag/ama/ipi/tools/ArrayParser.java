package fr.imag.ama.ipi.tools;

import java.util.ArrayList;
import java.util.regex.Pattern;

public class ArrayParser {

	public static String[] parse( String tree ) throws InvalidArrayException {
		tree = tree.trim();
		tree = tree.substring( 1, tree.length() - 1 );
		ArrayList<String> data = new ArrayList<String>();
		int index = 0;
		while( !"".equals( tree ) ) {
			if( tree.charAt( 0 ) == '"' ) {
				String part = tree.substring( 1, tree.indexOf( '"', 1 ) );
				data.add( part );
				tree = tree.substring( part.length() + 2 );
				if( !"".equals( tree ) ) {
					tree = tree.substring( 1 );
				}
				index = 0;
				continue;
			}
			
			index = tree.indexOf( ",", index );
			
			if( index == -1 ) {
				data.add( tree );
				tree = "";
				continue;
			}
			
			String part = tree.substring( 0, index ).trim();

			if( !checkOpenClose( part, Pattern.quote("{"), Pattern.quote("}") )
				|| !checkOpenClose( part, Pattern.quote("["), Pattern.quote("]") ) 
				|| !checkOpenClose( part, Pattern.quote("("), Pattern.quote(")") ) ) {
				index++;
				continue;
			}
			
			data.add( part );
			tree = tree.substring( index +1 );
			index = 0;
		}
		return data.toArray( new String[ data.size() ]);
	}
	
	private static boolean checkOpenClose( String tree, String open, String close ) {
		int length = tree.length();
		int nOpen = length - tree.replaceAll( open, "" ).length();
		int nClose = length - tree.replaceAll( close, "" ).length();
		return nClose == nOpen;
	}
}
